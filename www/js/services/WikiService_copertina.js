angular.module('LiveEngine.WikiCopertina.Services', [])

.service('WikiService_copertina', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, GlobalVariables, FileSystemService, SupportServices, LazyLoader, WikiService, LanguageService, $state) {

    var me = this;

    //me.toDownload = new Array();

    me.callbacks = [];


    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }
    }


    me.loadARchitectWorld = function (example)
    {
        console.log("Entrato in loadARchitectWorldCopertine");
        console.log(JSON.stringify(example));
        // check if the current device is able to launch ARchitect Worlds

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
             	console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);

                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed" + error);
                    //alert("Errore nell'avvio della Realtà Aumentata. Contattare l'assistenza");
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA'), true);
               },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.wtc+'","'+example.baseUrl+'","'+example.numUtenti+'","'+example.colore+'","'+GlobalVariables.systemLanguage+'")');
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
        	        {
                        var parse = WikiService.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "copertina":
                                console.log("Ha riconosciuto la copertina del progetto: "+ parse.parameter);
                                GlobalVariables.wikitudePlugin.close();
                                GlobalVariables.application.currentProgetto = {
                                    idProgetto: parse.parameter
                                };

                                $ionicLoading.show({
                                  template: LanguageService.getLabel('ATTENDERE')
                                });

                                SupportServices.sceltaProgetto({
                                  onComplete: function(err, json) {

                                      $ionicLoading.hide();
                                      if(err) {
                                          var alertPopup = $ionicPopup.alert({
                                             title: LanguageService.getLabel('ATTENZIONE'),
                                             template: LanguageService.getLabel('PROBLEMI_RETE')
                                           });
                                            $state.go('navigazione_progetti');
                                      }
                                      $state.go('progetto_detail');
                                  }
                                });
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "nonRilevo":
                                GlobalVariables.wikitudePlugin.close();
                                $state.go('navigazione_progetti');
                                break;
                             default:
                                GlobalVariables.wikitudePlugin.close();
                                $state.go('navigazione_progetti');
                        }
        	    	});

            }, function(errorMessage)
            {
            	console.log("Device is NOT supported");
                //alert("Device is NOT supported");
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO'), true);
            },
            example.requiredFeatures
            );


        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA'), true);
        },
        example.requiredFeatures
        );


    }





})
