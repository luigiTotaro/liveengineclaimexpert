angular.module('LiveEngine.WikiConfigurable.Services', [])

.service('WikiService_configurable', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, GlobalVariables, FileSystemService, SupportServices, LazyLoader, LanguageService, $state) {

    var me = this;

    var myParent;

    me.callbacks = [];

    me.setMyParent = function(parent)
    {
        myParent=parent;
    }

    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }

    }


    me.loadARchitectWorld = function (example)
    {
        console.log("Entrato in loadARchitectWorldConfigurable");
        console.log(JSON.stringify(example));
        // check if the current device is able to launch ARchitect Worlds

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);

                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed" + error);
                    //alert("Errore nell'avvio della Realtà Aumentata. Contattare l'assistenza");
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA'), true);
               },
                example.path, example.requiredFeatures, example.startupConfiguration
                );

                GlobalVariables.wikitudePlugin.callJavaScript("passData('"+example.localPath+"','"+example.wtc+"','"+example.game_key+"','"+example.labels+"','"+example.language+"')");    

                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url) {

                        var parse = myParent.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "messageFromAr":
                                if(parse.parameter.key) {
                                     
                                    for(var functionName in myParent.callbacks[parse.parameter.key]) {
                                        myParent.callbacks[parse.parameter.key][functionName](parse.parameter);
                                    }
                                }
                                break;
                            case "startFind":
                                break;
                            case "endFind":
                                break;
                            case "abandon":
                                console.log("Abbandono e Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                break;
                            default:
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('progetto_detail', null, {reload: true, notify:true});
                                }
                        }

                    });

            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert("Device is NOT supported");
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO'), true);
            },
            example.requiredFeatures
            );


        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA'), true);
        },
        example.requiredFeatures
        );


    }



})
