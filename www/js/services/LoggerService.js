angular.module('LiveEngine.Logger.Services', [])

.service('LoggerService', function($rootScope, $http, $ionicModal, $ionicPopup) {

      var me = this;

      me.CONTROLLER_STATES = {
          'HOME': 'HOME',
          'DETAIL_TESTATA': 'DETAIL_TESTATA',
          'LISTA_PROGETTI': 'LISTA_PROGETTI',
          'DETAIL_PROGETTO': 'DETAIL_PROGETTO',
          'WIKI_STATE': 'WIKI_STATE'
      };

      me.ACTION = {
          'ENTER': 'ENTER',
          'LEAVE': 'LEAVE',
          'TRIGGER_WIKI_EVT': 'TRIGGER_WIKI_EVT',
          'DATA_LOAD': 'DATA_LOAD',
          'OPEN_EXTERNAL_URL': 'OPEN_EXTERNAL_URL',
          'OPEN_AUGMENTED_CONTENT': 'OPEN_AUGMENTED_CONTENT' 
      };

      me.KNOWN_ERROR = {
          'IS_IN_BROWSER': 'IS_IN_BROWSER'
      };

      me.callbacks = [];


      me.setCallback = function(state, action, callback_name, callback) {

          if(!me.CONTROLLER_STATES[state]) {
            throw "Lo stato '" + state + "' non esiste.";
          }

          if(!me.ACTION[action]) {
            throw "La action '" + action + "' non esiste.";
          }

          if(!me.callbacks[state]) {
              me.callbacks[state] = [];
          }

          if(!me.callbacks[state][action]) {
              me.callbacks[state][action] = [];
          }

          if(me.callbacks[state][action][callback_name]) {
              delete me.callbacks[state][action][callback_name];
          }

          if(callback) {
              me.callbacks[state][action][callback_name] = callback;
          }

      };



      me.triggerAction = function(params) {

          if(!me.CONTROLLER_STATES[params.state]) {
            throw "Lo stato '" + params.state + "' non esiste.";
          }

          if(!me.ACTION[params.action]) {
            throw "La action '" + params.action + "' non esiste.";
          }

          console.log("Triggered " + params.action + " on " + params.state);

          if(me.callbacks[params.state] && me.callbacks[params.state][params.action]) {

              for(var cbName in me.callbacks[params.state][params.action]) {

                  me.callbacks[params.state][params.action][cbName](params.data);
              }

          }

      };


})
