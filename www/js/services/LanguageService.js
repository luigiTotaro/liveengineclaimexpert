angular.module('LiveEngine.Language.Services', [])

.service('LanguageService', function(GlobalVariables, $rootScope, $http, $ionicModal, $ionicPopup) {

    var me = this;
    
    me.getLabel = function(codice) {

        //console.log("getLabel: " + codice);
//        lingua=GlobalVariables.systemLanguage;

        var returnValue = "no-value";

        var token = _.find(GlobalVariables.systemLabels, function(item) { return item.codice == codice; });

         if(token) {
            returnValue = token[GlobalVariables.systemLanguage] ? token[GlobalVariables.systemLanguage] : token[GlobalVariables.systemLanguage + 'Default'];
        }

        if (returnValue == "no-value")
        {
          //console.log("non sono riuscito a tradurre: " + codice);

          //non è riuscito a tradurre.... ultima possibilità, non è riuscito a caricare il file delle traduzioni
          if (codice=="CHIUDERE_APPLICAZIONE_RIPROVARE")
          {
            returnValue = "Close the app and try again";
            if (GlobalVariables.systemLanguage=="it") returnValue = "Chiudere l'applicazione e riprovare";
            else if (GlobalVariables.systemLanguage=="fr") returnValue = "Fermez l'application et réessayez";
          }
          else if (codice=="ATTENZIONE")
          {
            returnValue = "Caution";
            if (GlobalVariables.systemLanguage=="it") returnValue = "Attenzione";
            else if (GlobalVariables.systemLanguage=="fr") returnValue = "Attention";
          }
          else if (codice=="INIZIA")
          {
            returnValue = "Start";
            if (GlobalVariables.systemLanguage=="it") returnValue = "Inizia";
            else if (GlobalVariables.systemLanguage=="fr") returnValue = "Commence";
          }
          else if (codice=="ATTENDERE")
          {
            returnValue = "Please Wait";
            if (GlobalVariables.systemLanguage=="it") returnValue = "Attendere";
            else if (GlobalVariables.systemLanguage=="fr") returnValue = "Veuillez patienter";
          }
          else if (codice=="MESSAGGIO_INIZIALE")
          {
            returnValue = "Dear User, <br> Welcome to Claim Expert. <br> You will soon be required to have access to the device files (or to the SD) to allow the App to download data and save it on the device. These data are necessary for interacting with Claim Expert. Thanks and good reading.";
            if (GlobalVariables.systemLanguage=="it") returnValue = "Gentile Utente,<br>benvenuto in Claim Expert.<br>A breve ti verrà richiesto il consenso di accedere ai file del dispositivo (o alla SD) per permettere alla App di scaricare dati e salvarli sul dispositivo. Questi dati sono necessari per interagire con Claim Expert.<br>Grazie e buona lettura.";
            else if (GlobalVariables.systemLanguage=="fr") returnValue = "Cher utilisateur, <br> Bienvenue dans Claim Expert. <br> Vous aurez bientôt besoin d'avoir accès aux fichiers de l'appareil (ou à la carte SD) pour permettre à l'application de télécharger des données et de les sauvegarder sur l'appareil. Ces données sont nécessaires pour interagir avec Claim Expert. Merci et bonne lecture.";
          }





        }
        //console.log("ritorno: " + returnValue);

        return returnValue;

    };

    me.getLabelParam = function(codice,params) {

        //console.log("getLabelParam: " + codice + " - " + JSON.stringify(params));
//        lingua=GlobalVariables.systemLanguage;

        var returnValue = "no-value";

        var token = _.find(GlobalVariables.systemLabels, function(item) { return item.codice == codice; });

         if(token) {
            returnValue = token[GlobalVariables.systemLanguage] ? token[GlobalVariables.systemLanguage] : token[GlobalVariables.systemLanguage + 'Default'];
        }

        if (returnValue != "no-value")
        {
          //sostituisco
          if (params)
          {
            for (var i=0;i<params.length;i++)
            {
              returnValue = returnValue.replace("{"+(i+1)+"}", params[i]);
            }
          }
        }
        //console.log("ritorno: " + returnValue);

        return returnValue;

    };


    me.getSystemLanguage = function() {

      console.log("getSystemLanguage");
      
      if(!window.cordova) {
          var systemLanguage="it";
          //il controllo se è una di quelle supportate lo faccio solo se è riuscito a caricare il json di configurazione
          if (GlobalVariables.supportedLanguages)
          {
            console.log("Ci sono le lingue supportate")
            //controllo se è una di quelle supportate
            //di base imposto l'italiano
            GlobalVariables.systemLanguage="it";
            for (var i=0;i<GlobalVariables.supportedLanguages.length;i++)
            {
              if (systemLanguage==GlobalVariables.supportedLanguages[i].codice) GlobalVariables.systemLanguage=systemLanguage;
            }
            console.log(GlobalVariables.systemLanguage);
          }
          else
          {
            console.log("Non ci sono le lingue supportate")
            GlobalVariables.systemLanguage=systemLanguage;
            console.log(GlobalVariables.systemLanguage);
          }
          //alert(GlobalVariables.systemLanguage);
          return true;
      }

      if (navigator.globalization)
      {
        console.log("navigator.globalization esiste");
        navigator.globalization.getPreferredLanguage(
            function (language) {    
                //alert('language: ' + language.value + '\n');
                //alert(JSON.stringify(language));
                //il formato è sempre it-IT, en-GB, en-US, fr-FR, de-DE
                var language=language.value;
                language=language.substring(0,2);

                //il controllo se è una di quelle supportate lo faccio solo se è riuscito a caricare il json di configurazione
                if (GlobalVariables.supportedLanguages)
                {
                  console.log("Ci sono le lingue supportate")
                  //controllo se è una di quelle supportate
                  //di base imposto l'italiano
                  GlobalVariables.systemLanguage="it";
                  for (var i=0;i<GlobalVariables.supportedLanguages.length;i++)
                  {
                    if (language==GlobalVariables.supportedLanguages[i].codice) GlobalVariables.systemLanguage=language;
                  }
                  console.log(GlobalVariables.systemLanguage);
                }
                else
                {
                  console.log("Non ci sono le lingue supportate")
                  GlobalVariables.systemLanguage=language;
                  console.log(GlobalVariables.systemLanguage);
                }
                //alert(GlobalVariables.systemLanguage);
            },
            function () {
                //alert('Error getting language\n');
                GlobalVariables.systemLanguage="it";
                //alert(GlobalVariables.systemLanguage);
            }
        );
      }
      else
      {
        console.log("navigator.globalization NON esiste");
        GlobalVariables.systemLanguage="it";
        return true;
      }
    };    


})
