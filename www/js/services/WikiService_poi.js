angular.module('LiveEngine.WikiPoi.Services', [])

.service('WikiService_poi', function(LoggerService, HelperService, $rootScope, $http, $ionicModal, $ionicPopup, $ionicLoading, GlobalVariables, FileSystemService, SupportServices, LazyLoader, WikiService, LanguageService, $state) {

    var me = this;

    me.callbacks = [];

    me.registerCallback = function(key, functionName, callback) {

        if(!me.callbacks[key]) {
            me.callbacks[key] = [];
        }

        delete me.callbacks[key][functionName];

        if(callback) {
            me.callbacks[key][functionName] = callback;
        }

    }


    me.loadARchitectWorld = function (example)
    {
        console.log("Entrato in loadARchitectWorld Percorso");

        GlobalVariables.wikitudePlugin.requestAccess(function()
        {
            console.log("Request Access OK");

            // check if the current device is able to launch ARchitect Worlds
            GlobalVariables.wikitudePlugin.isDeviceSupported(function()
            {
                console.log("Device is supported");
               //app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);


                GlobalVariables.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL)
                {
                    //console.log("loadARworld ok, call calljavascript: "+example.localPath);
                    /* Respond to successful world loading if you need to */

                    console.log("dovrei averla chiamata....");

                }, function errorFn(error)
                {
                    console.log("Loading AR web view failed");
                    //alert('Loading AR web view failed');
                    HelperService.showUnrecoverableError(LanguageService.getLabel('ERRORE_AVVIO_RA'), true);
                },
                example.path, example.requiredFeatures, example.startupConfiguration
                );
                GlobalVariables.wikitudePlugin.callJavaScript('passData("'+example.localPath+'","'+example.localDynamicARPath+'","'+example.idPercorso+'",'+example.idCliente+',"'+example.baseUrl+'","'+example.deviceId+'",'+LazyLoader.appConfiguration.showDisclaimerForComments+',"'+LazyLoader.appConfiguration.commentsNickname+'",'+example.position.coords.latitude+','+example.position.coords.longitude+','+example.position.coords.altitude+',"'+GlobalVariables.systemLanguage+'")');
                GlobalVariables.wikitudePlugin.setOnUrlInvokeCallback(function (url)
                    {
                        var parse = WikiService.parseWikitudeCallback(url);
                        switch(parse.value) {
                            case "storage":
                                break;
                            case "scaricaImmagine":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.TRIGGER_WIKI_EVT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        type: 'download_item',
                                        url: parse.parameter.url,
                                        descr: parse.parameter.descr
                                    }
                                });
                                break;
                            case "salvaConfig":
                                LazyLoader.appConfiguration.showDisclaimerForComments = parse.parameter.showCommentsDisclaimer;
                                LazyLoader.appConfiguration.commentsNickname = parse.parameter.commentsNickname;
                                LazyLoader.saveConfiguration({
                                    onComplete: function(err) {
                                    }
                                })
                                break;
                            case "linkEsterno":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_EXTERNAL_URL,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        url: parse.parameter
                                    }
                                });
                                break;
                            case "checkConnessione":
                                var connessione=HelperService.isNetworkAvailable();
                                console.log("Check connessione: " + connessione);
                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessione('+connessione+')');
                                break;
                            case "getUsersNumber":
                                var numero=GlobalVariables.application.currentProgetto.numUtenti;
                                GlobalVariables.wikitudePlugin.callJavaScript('getUsersNumber('+numero+')');
                                break;
                            case "scaricaSlideshow":
                                //nell'oggetto ci sta l'id del punto e le immagini da scaricare
                                WikiService.toDownload = new Array();
                                var idPunto=parse.parameter[0];
                                for (var index=1;index<parse.parameter.length;index++)
                                {
                                  WikiService.toDownload.push(parse.parameter[index]);
                                }
                                console.log("array da scaricare");
                                console.log(WikiService.toDownload);
                                WikiService.downloadFiles(0,idPunto);
                                break;
                            case "existSlideshow":
                                //esiste, nella directory slideshow, qualche file che inizia con l'id del punto?
                                var name=parse.parameter+"_0"; //almeno il primo
                                FileSystemService.fileExists({
                                    directory: SupportServices.BUFFER_DIRECTORY + '/slideshow',
                                    fileName: name,
                                    onComplete: function(err, isAvailable) {
                                        if(err) {
                                            //errore, getto la spugna
                                            console.log("Errore, comunico che non ci sono files");
                                            GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                        } else {
                                            if(!isAvailable) {
                                                 //non esiste, getto la spugna
                                                console.log("Non Esiste, comunico che non ci sono files");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(false)');
                                            } else {
                                                //esiste, vado avanti
                                                console.log("Esiste");
                                                GlobalVariables.wikitudePlugin.callJavaScript('checkConnessioneSlideshow(true)');
                                            }
                                        }
                                    }
                                });
                                break;
                            case "openContent":
                                LoggerService.triggerAction({
                                    action: LoggerService.ACTION.OPEN_AUGMENTED_CONTENT,
                                    state: LoggerService.CONTROLLER_STATES.WIKI_STATE,
                                    data: {
                                        id: parse.parameter.id,
                                        titolo: parse.parameter.titolo
                                    }
                                });
                                break;
                            case "naviga":
                                if ( device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos" ){
                                    // Google Maps (Android)
                                    var ref = window.open('http://maps.google.com/maps?q=' + parse.parameter.lat + '+' + parse.parameter.lon, '_system', 'location=yes');
                                } else {
                                    // Apple
                                    window.location.href = 'maps://maps.apple.com/?q=' + parse.parameter.lat + '+' + parse.parameter.lon;
                                }
                                break;
                            case "socialPublish":
                                var oggetto=parse.parameter;

                                GlobalVariables.wikitudePlugin.hide();
                                
                                $ionicLoading.show({
                                    template: '<div style="padding:10px 5px;">Attendere.....</div><ion-spinner icon="ripple" class="spinner-light"></ion-spinner>',
                                    duration: 15000
                                });

                                var appParameter="";

                                if (oggetto.type=="fb")
                                {
                                    
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.facebook";
                                    else appParameter="com.facebook.katana";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per fb');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                           window.plugins.socialsharing.shareViaFacebook(null, oggetto.image, oggetto.link, function()
                                            {
                                                console.log('share ok fb');
                                                WikiService.showPopupReturn2AR("fb");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore fb: ' + errormsg);
                                                WikiService.showPopupReturn2AR("fb");
                                            });
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("fb_error");
                                            $ionicLoading.hide();
                                        });
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="tw")
                                {
                                    if (GlobalVariables.platform=="iOS") appParameter="com.apple.social.twitter";
                                    else appParameter="twitter";

                                     setTimeout(function() {
                                        console.log('chiamo il plugin di share per tw');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaTwitter(oggetto.text, oggetto.image, "", function()
                                            {
                                                console.log('share ok tw');
                                                WikiService.showPopupReturn2AR("tw");
                                            }, function(errormsg)
                                            {
                                                console.log('Errore tw: ' + errormsg);
                                                WikiService.showPopupReturn2AR("tw");
                                            });  
                                 
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("tw_error");
                                            $ionicLoading.hide();
                                        });
                                        console.log("dovrei aver fatto");
                                    }, 1000);                              
                                }
                                else if (oggetto.type=="insta")
                                {

                                    if (GlobalVariables.platform=="iOS") appParameter="instagram";
                                    else appParameter="instagram";

                                    setTimeout(function() {
                                        console.log('chiamo il plugin di share per instagram');
                                        window.plugins.socialsharing.canShareVia(appParameter, oggetto.text, null, oggetto.image, null, function(e)
                                        {
                                            console.log("success: " + e)
                                            window.plugins.socialsharing.shareViaInstagram(oggetto.text, oggetto.image, function()
                                            {
                                              console.log('share ok insta');
                                                WikiService.showPopupReturn2AR("insta");
                                                //GlobalVariables.wikitudePlugin.show();
                                            }, function(errormsg)
                                            {
                                              console.log('Errore insta: ' + errormsg);
                                                WikiService.showPopupReturn2AR("insta");
                                                 //GlobalVariables.wikitudePlugin.show();
                                           });                               
                                        }, function(e)
                                        {
                                            console.log("KO: " + e);
                                            WikiService.showPopupReturn2AR("insta_error");
                                            $ionicLoading.hide();
                                        });
                                     }, 1000);                              
                                }
                                break;
                            case "writeComment":
                                GlobalVariables.wikitudePlugin.hide();
                                WikiService.showPopupWriteComment(parse.parameter);
                                break;
                            default:
                                console.log("Esco dalla RA");
                                try{
                                    GlobalVariables.wikitudePlugin.close();
                                    console.log("dovrei averla chiusa");
                                    $state.transitionTo('poi_detail', null, {reload: true, notify:true});
                                }
                                catch(err){
                                    console.log(err);
                                    $state.transitionTo('poi_detail', null, {reload: true, notify:true});
                                }
                        }
                    });

            }, function(errorMessage)
            {
                console.log("Device is NOT supported");
                //alert(errorMessage);
                HelperService.showUnrecoverableError(LanguageService.getLabel('DEVICE_NON_SUPPORTATO'), true);
            },
            example.requiredFeatures
            );

        }, function(errorMessage)
        {
            console.log(errorMessage);
            //alert("Non riesco ad accedere alla Fotocamera");
            HelperService.showUnrecoverableError(LanguageService.getLabel('NO_ACCESSO_FOTOCAMERA'), true);
        },
        example.requiredFeatures
        );



    }




})
