angular.module('LiveEngine.Helper.Services', [])


.service('GuideServiceHelper', function(GlobalVariables, $rootScope, $timeout,
$http, $ionicModal, $ionicPopup, ionImgCacheSrv, SupportServices, LanguageService, LazyLoader) {

  var self = this;

  self.showUsersGuide = function(error, hasCloseButton) {

      var usersGuideScope = $rootScope.$new(true);

      usersGuideScope.closeUsersGuideWnd = function() {

          usersGuideScope.usersGuideWnd.remove();
          usersGuideScope.$destroy();
          delete usersGuideScope;

      };

      usersGuideScope.slide_list = [];

      usersGuideScope.options = {
          loop: false,
          effect: 'slide',
          speed: 500,
      }

      usersGuideScope.usersGuideWnd = $ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" style="background-color:rgba(0,0,0,0.5); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              '<ion-content scroll="false" padding="false" style="background-image: url(img/background_grey.jpg);">' +

                  '<a ng-click="closeUsersGuideWnd()" style="position:absolute; top: 5px; right: 5px; z-index:99999;" class="button button-icon icon ion-ios-close activated"></a>' +

                  '<ion-slides options="options" slider="data.slider">' +

                      //slides +
                      '<ion-slide-page ng-repeat="current_slide in slide_list">' +

                          '<ion-slide-page>'+

                            '<div id="guide_{{$index}}" class="box" ion-img-cache-bg style="background-repeat: no-repeat; background-position: center center; background-size:contain; background-image:url({{current_slide.url}}); height:100%;"></div>'+

                          '</ion-slide-page>' +

                      '</ion-slide-page>' +

                  '</ion-slides>' +

              '</ion-content>' +
          '</ion-modal-view>', {
              scope: usersGuideScope,
              animation :'none',
              hardwareBackButtonClose: false
          }

    );



    usersGuideScope.usersGuideWnd.show().then(function() {

        SupportServices.getGuideImageLinks({
          onComplete: function(err, slides) {

              if(err) {

                  usersGuideScope.closeUsersGuideWnd();

                  $ionicPopup.alert({
                     title: LanguageService.getLabel('ATTENZIONE') + ' <i class="ion-android-alert"></i>',
                     template: LanguageService.getLabel('PROBLEMI_RETE')
                  });

              } else {

                  _.each(slides, function(item) {
                      item.url = GlobalVariables.baseUrl + "/" + item.url;
                  });

                  $timeout(function() {
                      usersGuideScope.slide_list = slides;
                  });

                  if(window.cordova && LazyLoader.appConfiguration.showUsersGuideAtStartup == true) {

                        $ionicPopup.confirm({
                          title: GlobalVariables.applicationTitle,
                          template: LanguageService.getLabel('MOSTRARE_GUIDA_PROSSIMO_AVVIO'),
                          cancelText: LanguageService.getLabel('NO'), // String (default: 'Cancel'). The text of the Cancel button.
                          cancelType: 'button-stable', // String (default: 'button-default'). The type of the Cancel button.
                          okText: LanguageService.getLabel('SI'), // String (default: 'OK'). The text of the OK button.
                          okType: 'button-positive', // String (default: 'button-positive'). The type of the OK button.
                        }).then(function(res) {

                           if(res) {
                              LazyLoader.appConfiguration.showUsersGuideAtStartup = true;
                           } else {
                             LazyLoader.appConfiguration.showUsersGuideAtStartup = false;
                           }

                           LazyLoader.saveConfiguration({
                             onComplete: function(err) {}
                           });

                       });

                  }

              }

            }

        });

    });

  };

})


.service('WelcomeScreenHelper', function(GlobalVariables, $rootScope,
$timeout,HelperService, $http, $ionicModal, $ionicPopup, ionImgCacheSrv,
SupportServices, LazyLoader, LoggerService, LanguageService, $ionicLoading) {

      console.log("instanziato il WelcomeScreenHelper");
      var self = this;
      self.welcomePageScope = null;


      // ##################################################################### //
      // chiusura dello splash screen e deallocazione della variabile di scope //
      // ##################################################################### //
      self.closeSplashScreen = function() {
          if (self.welcomePageScope.splashScreen) self.welcomePageScope.splashScreen.remove();
          self.welcomePageScope.$destroy();
          delete self.welcomePageScope;
      };

      // ########################################### //
      // inizializzazione e show dello splash screen //
      // ########################################### //
      self.showSpashScreen = function(parameters) {

        console.log("WelcomeScreenHelper.showSpashScreen");

        //determino comunque la lingua di sistema, anche se non ho ancora caricato il file delle traduzioni e delle lingue supportate
        LanguageService.getSystemLanguage();

        LazyLoader.Init({
          onComplete: function(err) {


              if(err && err != LoggerService.KNOWN_ERROR.IS_IN_BROWSER) {

                  HelperService.showUnrecoverableError(err, false);

              } else {

                  self.welcomePageScope = $rootScope.$new(true);

                  if( window.cordova && LazyLoader.appConfiguration.showDisclaimerAtStartup == true ) {

                      self.isDisclaimerAccepted = false;

                      // ########################################## //
                      // welcome screen CON accettazione disclaimer //
                      // ########################################## //
                      self.welcomePageScope.triggerOnClick = function() {

                          LazyLoader.appConfiguration.showDisclaimerAtStartup = false;

                          LazyLoader.saveConfiguration({
                            onComplete: function(err) {
                              if(parameters.onClose) {
                                  parameters.onClose();
                              }
                            }
                          })

                      };


                      $ionicLoading.show({
                          template: LanguageService.getLabel('ATTENDERE')
                      });

                      self.welcomePageScope.splashScreen = $ionicModal.fromTemplate(

                          '<ion-modal-view padding="false" cache-view="false" id="appFirstWelcomeScreenDisclaimer">' +

                              '<ion-header-bar align-title="center">' +
                                '<h3 style="width:100%; text-align:center;">'+LanguageService.getLabel('TERMINI_CONDIZIONI')+'</h3>' +
                              '</ion-header-bar>' +

                              '<ion-content scroll="true" overflow-scroll="true" padding="true" class="has-header">' +
                                  '<iframe id="disclaimerLoader" src=""></iframe>' +
                              '</ion-content>' +

                              '<ion-footer-bar align-title="left">' +
                                  '<div id="row1">' +
                                      '<ion-checkbox style="" ng-model="isDisclaimerAccepted">'+LanguageService.getLabel('ACCETTO')+'</ion-checkbox>' +
                                  '</div>' +
                                  '<div id="row2">' +
                                    '<button ng-disabled="!isDisclaimerAccepted" ng-click="triggerOnClick()" class="button button-full button-positive cst-button" style="background-color:#b41223;">'+LanguageService.getLabel('ACCETTO')+'</button>' +
                                  '</div>' +
                              '</ion-footer-bar>' +

                          '</ion-modal-view>',

                          {
                              scope: self.welcomePageScope,
                              animation :'none',
                              hardwareBackButtonClose: false
                          }

                      );

                      self.welcomePageScope.splashScreen.show().then(function() {

                        document.getElementById('disclaimerLoader').onload = function() {
                            setTimeout(function(){
                                $ionicLoading.hide();
                            }, 200);
                        }

                        if(HelperService.isNetworkAvailable() == true) {
                            document.getElementById('disclaimerLoader').src = GlobalVariables.baseUrl + "/disclaimers/" + GlobalVariables.idCliente + "/disclaimer.html";
                        }

                      });




                      if(HelperService.isNetworkAvailable() == false) {
                          HelperService.showUnrecoverableError(LanguageService.getLabel('NO_CONNESSIONE_PRIMO_AVVIO'), false);
                      }
                      // ########################################## //
                      // welcome screen CON accettazione disclaimer //
                      // ########################################## //

                  } else {


                      // ############################################ //
                      // welcome screen SENZA accettazione disclaimer //
                      // ############################################ //
                      self.welcomePageScope.triggerOnClick = function() {
                          
                          $ionicLoading.show({
                              template: '<ion-spinner icon="ripple" class="spinner-light"></ion-spinner>'
                          });

                          if(parameters.onClose) {
                              parameters.onClose();
                          }
                      };

                      //se devo mostrare il messaggio mostro tutta la pagina, altrimenti vado dritto alle testate
                      if (parameters.showMessage)
                      {
                        var myclass="";
                        var mycss="";
                        /*
                        if (parameters.showMessage)
                        {
                          myclass="";
                          mycss="";
                        }
                        else
                        {
                          myclass="has-footer";
                          mycss="display:none;";
                        }
                        */


                        self.welcomePageScope.splashScreen = $ionicModal.fromTemplate(

                            '<ion-modal-view padding="true" cache-view="false" id="appFirstWelcomeScreen">' +

                                '<ion-content id="messageScrollableArea" scroll="false" padding="false" style="text-align:center;" class="'+myclass+'">' +

                                    '<div style="width:100%; height:100%;">' +
                                        '<img class="logoSplash" style="vertical-align: middle;" src="img/logo_splash_screen.png">' +

                                        '<div class="infoPermessi" style="'+mycss+'">' +
                                          LanguageService.getLabel('MESSAGGIO_INIZIALE') +
                                        '</div>' +


                                    '</div>' +

                                    '<ion-footer-bar align-title="left" class="splashScreenFooter">' +
                                        '<button ng-click="triggerOnClick()" class="button button-full button-positive cst-button" style="background-color:#b41223;">'+LanguageService.getLabel('INIZIA')+'</button>' +
                                    '</ion-footer-bar>' +
                                '</ion-content>' +

                            '</ion-modal-view>',

                            {
                                scope: self.welcomePageScope,
                                animation :'none',
                                hardwareBackButtonClose: false
                            }

                        );

                        self.welcomePageScope.splashScreen.show();
                      }
                      else
                      {
                        self.welcomePageScope.triggerOnClick();
                      }

                      // ############################################ //
                      // welcome screen SENZA accettazione disclaimer //
                      // ############################################ //

                  }


              }

          }

      });

    };

})

.service('HelperService', function(GlobalVariables, $rootScope, $http, $ionicModal, $ionicPopup, $state, ionImgCacheSrv, LanguageService, $window) {

    var me = this;

    me.isNetworkAvailable = function() {

      if(!window.cordova) {
          return true;
      }

      var networkState = navigator.connection.type;

      if(networkState == Connection.ETHERNET || networkState == Connection.WIFI
      || networkState == Connection.CELL_2G || networkState == Connection.CELL_3G
      || networkState == Connection.CELL_4G || networkState == Connection.CELL) {
          return true;
      } else {
          return false;
      }

    };





    me.showUnrecoverableError = function(error, hasCloseButton) {

        var buttons = [];

        if(hasCloseButton == true) {
            buttons.push({
              text: LanguageService.getLabel('CHIUDI'),
              type: 'button-assertive'
            });
        }

        $ionicPopup.show({
          template: error,
          title: '<i class="ion-android-alert app-error-icon"></i> '+LanguageService.getLabel('ATTENZIONE'),
          //subTitle: 'Si è verificato un errore',
          cssClass: 'application-error',
          buttons: buttons
        });

    };

    me.showGenericMessage = function(message, hasCloseButton) {

        var buttons = [];

        if(hasCloseButton == true) {
            buttons.push({
              text: LanguageService.getLabel('CHIUDI'),
              type: 'button-balanced'
            });
        }

        $ionicPopup.show({
          template: message,
          title: '<i class="ion-ios-information-outline"></i> Claim Expert AR+',
          //subTitle: 'Si è verificato un errore',
          cssClass: 'application-message',
          buttons: buttons
        });
    };

    me.showBlockingGenericMessage = function(message, chiudiText, returnFunction) {

        $ionicPopup.show({
          template: message,
          title: '<i class="ion-ios-information-outline"></i> Claim Exper AR+',
          cssClass: 'application-message',
          buttons: [
            {
              text: chiudiText,
              type: 'button-balanced',
              onTap: returnFunction
            }
          ]
        });
    };

    me.showCreditsScreen = function() {

        $rootScope.closeCreditsScreen = function() {
            $rootScope.creditsScreen.remove();
        };

        $rootScope.svuotaCache = function() {

            ionImgCacheSrv.clearCache().then(function() {

              //$window.location.reload(true)
              $ionicPopup.alert({
                 title: LanguageService.getLabel('OPERAZIONE_COMPLETATA'),
                 template: LanguageService.getLabel('CHIUDERE_RIAPRIRE_APPLICARE_MODIFICHE')
              });

            }).catch(function() {

              $ionicPopup.alert({
                 title: 'Errore'
              });

            });

        };

        $rootScope.openPage = function(destination) {

          try {
            var url = "";

            if(destination == 'claimexpert') {
                url = 'http://www.claimexpert.it/';
            }

            if(destination == 'mediasoft') {
              url = 'http://www.mediasoftonline.com/';
            }

            if(window.cordova && window.cordova.InAppBrowser){
                window.cordova.InAppBrowser.open(url, "_system");
            }else{
                window.open(url,'_blank');
            }

          } catch(err) {
            alert(err);
          }

        }

        var deviceData = {
          OS: GlobalVariables.platform,
          VERSION: GlobalVariables.version,
          BUILD: GlobalVariables.build,
          UID: GlobalVariables.deviceUUID,
          BRAND: GlobalVariables.manufacturer
        };


        $rootScope.creditsScreen = $ionicModal.fromTemplate(

          '<ion-modal-view padding="true" cache-view="false" id="appCreditsScreen">' +

              '<ion-content scroll="true" padding="false" >' +

                  '<div id="logoMondadoriBox">' +
                    '<img ng-click="openPage(\'claimexpert\');" src="img/logo_claimexpert_credits.png"></img>'+
                  '</div>'+

                  '<div id="boxInfoMondandori">' +
                    '<strong>Claim Expert SRL</strong>' +
                    '<br>Via P.Marti nr. 9/A - 73100 Lecce (LE)' +

                  '</div>' +

                  '<div id="logoMediasoftBox">' +
                    '<img ng-click="openPage(\'mediasoft\');" src="img/logoMediasoftCredits.png"></img>'+
                  '</div>'+

                  '<div id="boxInfoMediasoft">' +
                    'Powered by <strong>MediaSoft srl</strong>' +
                    '<br>www.mediasoftonline.com' +
                  '</div>' +


                  '<div id="deviceInfoBox">' +
                      '<strong>'+LanguageService.getLabel('INFORMAZIONI_DEVICE')+'</strong>' +
                      '<div><span class="infoLabel">'+LanguageService.getLabel('MARCA')+':</span> <span class="infoValue">' + deviceData.OS + '</span></div>'+
                      '<div><span class="infoLabel">'+LanguageService.getLabel('SISTEMA_OPERATIVO')+':</span> <span class="infoValue">' + deviceData.OS + '</span></div>'+
                      '<div><span class="infoLabel">'+LanguageService.getLabel('VERSIONE')+':</span> <span class="infoValue">' + deviceData.VERSION + '</span></div>'+
                      '<div><span class="infoLabel">'+LanguageService.getLabel('BUILD')+':</span> <span class="infoValue">' + deviceData.BUILD + '</span></div>' +
                      '<div><span class="infoLabel">'+LanguageService.getLabel('DEVICE_ID')+':</span> <span class="infoValue">' + deviceData.UID + '</span></div>' +

                      (GlobalVariables.isDebugDevice == true ? '<div><button ng-click="svuotaCache()" class="icon-left ion-trash-a button button-full button-positive cst-button">'+LanguageService.getLabel('SVUOTA_CACHE_IMMAGINI')+'</button></div>' : '') +

                  '</div>'+

              '</ion-content>' +


              '<ion-footer-bar align-title="left" class="splashScreenFooter">' +
                  '<button ng-click="closeCreditsScreen()" class="button button-full button-positive cst-button" style="background-color: #052ad7">'+LanguageService.getLabel('CHIUDI')+'</button>' +
              '</ion-footer-bar>' +

          '</ion-modal-view>',

          {
              scope: $rootScope,
              focusFirstInput: true,
              animation :'none',
              hardwareBackButtonClose: false
          }

      );

      $rootScope.creditsScreen.show();
 

    };

    me.showLanguageScreen = function() {

        $rootScope.setLanguage = function(lang) {
            $rootScope.languageScreen.remove();
            GlobalVariables.systemLanguage=lang;
            //dove sto sto devo ritornare alla home, per riaggiornare le label
            //$state.go('home');
            $state.transitionTo('home', null, {reload: true, notify:true});                
        };


        var listaLingue="";
        for (var i=0;i<GlobalVariables.supportedLanguages.length;i++)
        {
              listaLingue += '<ion-item style="background-color: rgba(255,255,255,0.5);" ng-click="setLanguage(\''+GlobalVariables.supportedLanguages[i].codice+'\')"><img src="'+GlobalVariables.baseUrl + '/' + GlobalVariables.supportedLanguages[i].flag + '" style="height: 26px;"><span style="padding-left:10px;vertical-align: super;">' + GlobalVariables.supportedLanguages[i].nome + '</span></ion-item>';
        }


        $rootScope.languageScreen = $ionicModal.fromTemplate(

          '<ion-modal-view padding="true" cache-view="false" id="appLanguageScreen">' +

              '<ion-content scroll="true" padding="false" >' +

                  '<div id="scegliLinguaText">' + LanguageService.getLabel('SCEGLI_LA_LINGUA') + '</div>'+

                  '<div id="scegliLinguaBox">' +
                    '<ion-list>' +
                      listaLingue +
                    '</ion-list>' +
                  '</div>'+

              '</ion-content>' +

          '</ion-modal-view>',

          {
              scope: $rootScope,
              focusFirstInput: true,
              animation :'none',
              hardwareBackButtonClose: false
          }

      );

      $rootScope.languageScreen.show();
 

    };    



})
