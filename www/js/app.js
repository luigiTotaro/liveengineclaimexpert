// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('LiveEngine', [
  'ionic',
  'ionicImgCache',
  'angularMoment',
  // services
  'LiveEngine.LazyLoader.Services',
  'LiveEngine.Helper.Services',
  'LiveEngine.Support.Services',
  'LiveEngine.FileSystem.Services',
  'LiveEngine.Logger.Services',
  'LiveEngine.Wiki.Services',
  'LiveEngine.WikiCopertina.Services',
  'LiveEngine.WikiPrincipale.Services',
  'LiveEngine.WikiPoi.Services',
  'LiveEngine.WikiConfigurable.Services',
  'LiveEngine.GeoLocation.Services',
  'LiveEngine.Language.Services',
  'LiveEngine.SocketService.Services',
  'LiveEngine.Push.Services',
  'LiveEngine.GameCenter.Services',
  // controllers
  'LiveEngine.Home.Controller',
//  'LiveEngine.TestataDetail.Controller',
  'LiveEngine.NavigazioneProgetti.Controller',
  'LiveEngine.ProgettoDetail.Controller',
  'LiveEngine.PoiDetail.Controller'

])


.constant('GlobalVariables', {

    version: 0.0,
    idCliente: 16,
    applicationTitle: 'Claim Expert AR+',
    baseUrl: 'http://livengine.mediasoftonline.com',
    systemLabels: [],
    wikitudePlugin : null,
    senderID: "729912079795",
    menuWnd: null,
    deviceUUID: 'browser',
    directory_trail: '_clex',
    isDebugDevice: false,

    platform: 'browser',
    version: 'browser',
    build: 'browser',
    manufacturer: 'unkwown',

    application: {
        applicationUrl: null,
        currentProgetto: null,
        currentTestata: null,
        welcomeScreenHasBeenShown: false,
        disableBackButtonToHome: false,
        currrentLayoutTestata: null,
        listaProgetti: [],
        poi: null,
        sendPushId2ServerSent: false
    }

})

.run(function($ionicPlatform, $rootScope, $state, $ionicModal, GlobalVariables
, SupportServices, HelperService, GeoLocationService, GuideServiceHelper, LanguageService
, amMoment) {

    $ionicPlatform.ready(function() {

            amMoment.changeLocale('it');

            if(window.cordova && ionic.Platform.isIOS()) {
               
                GlobalVariables.beepSound = new Media('sounds/chat.wav');     
            } else if (window.cordova && ionic.Platform.isAndroid()){
                
                $ionicPlatform.registerBackButtonAction(function (event) {
                    event.preventDefault();
                }, 100);

                GlobalVariables.beepSound = new Media('/android_asset/www/sounds/chat.mp3');
            } else {
               
                GlobalVariables.beepSound = new Audio('sounds/chat.wav')
            }

            $rootScope.variable_menu_items = [];

            



            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(false);
            }


            if(window.StatusBar) {

                StatusBar.styleDefault();

                if(ionic.Platform.isIOS()) {
                    ionic.Platform.fullScreen(true, false);
                }

            }


            $rootScope.showCredits = function() {
                $rootScope.closeMainMenu();
                HelperService.showCreditsScreen();
            };

            $rootScope.showLanguage = function() {
                $rootScope.closeMainMenu();
                HelperService.showLanguageScreen();
            };

            $rootScope.setLanguage = function(lang) {
                $rootScope.closeMainMenu();
                GlobalVariables.systemLanguage=lang;
                //dove sto sto devo ritornare alla home, per riaggiornare le label
                //$state.go('home');
                $state.transitionTo('home', null, {reload: true, notify:true});                
            };


            $rootScope.showUsersGuide = function() {
                $rootScope.closeMainMenu();
                GuideServiceHelper.showUsersGuide();
            };

            $rootScope.closeMainMenu = function() {
                $rootScope.menuWnd.remove();
            }

            $rootScope.removeItemFromMenu = function(menu_key) {
                delete $rootScope.variable_menu_items[menu_key];
            };


            $rootScope.addItemToMenu = function(menu_key, label, icon, callback) {

                $rootScope.variable_menu_items[menu_key] = {
                    label: label,
                    icon: icon,
                    callback: callback
                };

            };


            $rootScope.executeVariableMenuCallback = function(menu_key) {
                $rootScope.variable_menu_items[menu_key].callback()
            };


            // callback: function(isNetworkAvailable, isGpsAvailable, isGpsAuthorized)
            $rootScope.checkSystemConstraints = function(callback) {

                GeoLocationService.checkGpsAvailability(function(isGpsAvailable) {

                  callback(HelperService.isNetworkAvailable(), isGpsAvailable);

                });

            };



            $rootScope.showMainMenu = function() {

                var color1 = '#052ad7';

                if(GlobalVariables.application.currrentLayoutTestata) {
                  color1 = GlobalVariables.application.currrentLayoutTestata.coloreGenerale;
                }



                $rootScope.checkSystemConstraints(function(isNetworkAvailable, isGpsAvailable) {
                    //console.log("--- constraint: " + isNetworkAvailable + " - " + isGpsAvailable);

                    var variable_menu_fragment = "";

                    for(var menu_key in $rootScope.variable_menu_items) {

                        variable_menu_fragment +=
                          '<ion-item ng-click="executeVariableMenuCallback(\'' + menu_key + '\')">' +
                            '<i class="' + $rootScope.variable_menu_items[menu_key].icon + '"></i> <span style="padding-left:10px;">' + $rootScope.variable_menu_items[menu_key].label + '</span>' +
                          '</ion-item>';

                    }

                    var system_menu_items = "";

                    if(!isGpsAvailable) {

                        system_menu_items +=
                          '<ion-item class="no-gps-available">' +
                            '<i class="ion-android-compass"></i> <span style="padding-left:10px;">' +
                                LanguageService.getLabel('FUNC_GPS_NON_DISPONIBILE')+'<br><small class="small_descr">'+LanguageService.getLabel('APP_RICHIEDE_GPS')+'</small>' +
                            '</span>' +
                          '</ion-item>';

                    }

                    if(isNetworkAvailable == false) {

                        system_menu_items +=
                          '<ion-item class="no-internet-connection"><i class="ion-alert-circled"></i> <span style="padding-left:10px;">Connessione Internet Assente</span></ion-item>';

                    }

                    //lingue
                    /*
                    for (var i=0;i<GlobalVariables.supportedLanguages.length;i++)
                    {
                        system_menu_items +=
                          '<ion-item ng-click="setLanguage(\''+GlobalVariables.supportedLanguages[i].codice+'\')"><img src="'+GlobalVariables.baseUrl + '/' + GlobalVariables.supportedLanguages[i].flag + '" style="height: 26px;"><span style="padding-left:10px;vertical-align: super;">' + GlobalVariables.supportedLanguages[i].nome + '</span></ion-item>';
                    }
                    */


                    $rootScope.menuWnd = $ionicModal.fromTemplate(

                      '<ion-modal-view id="main-menu-wnd">' +

                          '<ion-header-bar style="background-color:' + color1 + ';">' +

                             '<h1 class="title cst-title">' + GlobalVariables.applicationTitle + '</h1>' +
                             '<i id="mainMenuCloseBtn" class="icon ion-close-circled placeholder-icon" ng-click="closeMainMenu()"></i>'+

                          '</ion-header-bar>' +


                          '<ion-content scroll="true" padding="true" class="has-header">' +

                              '<ion-list>' +

                                  '<ion-item ng-click="showCredits()"><i class="ion-at"></i> <span style="padding-left:10px;">'+LanguageService.getLabel('CREDITS')+'</span></ion-item>' +
                                  '<ion-item ng-click="showUsersGuide()"><i class="ion-university"></i><span style="padding-left:10px;">'+LanguageService.getLabel('GUIDA_ALL_USO')+'</span></ion-item>' +
                                  '<ion-item ng-click="showLanguage()"><i class="ion-earth"></i> <span style="padding-left:10px;">'+LanguageService.getLabel('LINGUA')+'</span></ion-item>' +

                                  variable_menu_fragment +

                                  '<ion-item><i class="ion-information-circled"></i> <span style="padding-left:10px;">'+LanguageService.getLabel('VERSIONE')+': ' + GlobalVariables.version + '</span></ion-item>' +

                                  system_menu_items +

                              '</ion-list>' +

                          '</ion-content>' +

                      '</ion-modal-view>',

                      {
                          scope: $rootScope,
                          animation: 'none'
                      }

                  );

                  $rootScope.menuWnd.show();

              });


            };

    });

})




.config(function(GlobalVariables, $ionicConfigProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider, ionicImgCacheProvider) {



  ionicImgCacheProvider.debug(false);

    // Set storage size quota to 100 MB.
    ionicImgCacheProvider.quota(300);

  $sceDelegateProvider.resourceUrlWhitelist([
      // Allow same origin resource loads.
      'self',
      // Allow loading from our assets domain.  Notice the difference between * and **.
      'http://ionicframework.com/**',
      GlobalVariables.httpWebServerUrl + "/**"
  ]);

  $ionicConfigProvider.views.transition('none');
  $ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.views.swipeBackEnabled(false);
  $ionicConfigProvider.tabs.style('striped');
  $ionicConfigProvider.tabs.position('bottom');



  $stateProvider

    .state('home', {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
    })

/*
    .state('testata_detail', {
        templateUrl: 'templates/testataDetail.html',
        controller: 'TestataDetailCtrl'
    })
*/

    .state('navigazione_progetti', {
        templateUrl: 'templates/navigazioneProgetti.html',
        controller: 'NavigazioneProgettiCtrl'
    })

    .state('progetto_detail', {
        templateUrl: 'templates/progettoDetail.html',
        controller: 'ProgettoDetailCtrl'
    })

    .state('poi_detail', {
        templateUrl: 'templates/poiDetail.html',
        controller: 'PoiDetailCtrl'
    });

  // if none of the above states are matched, use this as the fallback
  // $urlRouterProvider.otherwise('/tab/dash');


})


.controller('EntryPointController', function(FileSystemService, PushServices,
LoggerService, HelperService, SupportServices, LazyLoader, GlobalVariables, 
WelcomeScreenHelper, $scope, $state, $http, WikiService, GeoLocationService, 
$ionicLoading, GuideServiceHelper, SocketService, GameCenter, LanguageService) {

    ////////////////
    // real start //
    ////////////////
    var doStart = function() {

          GameCenter.Initialize();

          // #################################################################### //
          // inserisco un generico responder per l'evento di apertura url esterno //
          // #################################################################### //
          LoggerService.setCallback(LoggerService.CONTROLLER_STATES.WIKI_STATE, LoggerService.ACTION.OPEN_EXTERNAL_URL, 'openUrlFromWiki', function(data) {

              if(window.cordova && window.cordova.InAppBrowser) {
                  window.cordova.InAppBrowser.open(data.url, "_system");
              } else {
                  window.open(data.url,'_blank');
              }

          });


          LoggerService.setCallback(LoggerService.CONTROLLER_STATES.WIKI_STATE, LoggerService.ACTION.OPEN_AUGMENTED_CONTENT, 'openAugmentedContent', function(data) {

              SocketService.emit('message','has_open_augmented_content', {}, {
                  uid: GlobalVariables.deviceUUID,
                  id: data.id, // id del contenuto aumentato
                  id_progetto: GlobalVariables.application.currentProgetto.idProgetto, // ide della rivista al quale il contenuto aumentato è collegato
                  titolo: data.titolo // tuitolo del contenuto aumentato
              });

          });


          // ##################################################### //
          // sotto dispositivo devo assicurarmi che wikitude parta //
          // ##################################################### //
          if (window.cordova && !WikiService.setupCordova()) {
              HelperService.showUnrecoverableError('errore inizializzazione realtà aumentata', false);
              return;
          }

          //console.log("Arrivato");
          GeoLocationService.Initialize();
          PushServices.InitPushNotifications();

          // ############################################################################## //
          // al completamento richiedo il json di configurazione del cliente. es: le lingue //
          // ############################################################################## //
          SupportServices.getConfiguration({
              onComplete: function(err, jsonConfig) {

                  //console.log("on complete di getconfiguration");
                  if(err) {

                    //determino comunque la lingua di sistema
                    LanguageService.getSystemLanguage();
                    HelperService.showUnrecoverableError(err + '<br>' + LanguageService.getLabel('CHIUDERE_APPLICAZIONE_RIPROVARE'), false);
                    // non riesco ad avere un json valido in nessuna maniera. rrore grave e non recoverable

                  } else {


                      GlobalVariables.systemLabels = jsonConfig.labels;
                      GlobalVariables.supportedLanguages = jsonConfig.languages;
                      GlobalVariables.intervalloUtentiTestate = jsonConfig.intervalloUtentiTestate;
                      GlobalVariables.intervalloUtentiNumeri = jsonConfig.intervalloUtentiNumeri;

                      //determino la lingua di sistema, e imposto quella di base, ora che so quali sono quelle supportate
                      LanguageService.getSystemLanguage();
                      
                      //vedo se esiste una posizione calcolata dall'ip
                      /*
                      if (jsonConfig.ipDetails)
                      {
                        var ipCoords=jsonConfig.ipDetails.loc;
                        var arrayCoords=ipCoords.split(",");
                        GeoLocationService.setCurrentIpPosition(arrayCoords[0],arrayCoords[1]);
                      }*/


                      // ################################################################################## //
                      // a questo punto devo decidere se andare nella pagina con la lista delle copertine   //
                      // nel caso vi siano più riviste o soltare alla pagina dell'unica rivista disponibile //
                      // ################################################################################## //
                      $ionicLoading.show({
                          template: '<ion-spinner icon="ripple" class="spinner-light"></ion-spinner>' 
                      });


                      SupportServices.getListaCopertine({

                          onComplete: function(err, jsonData) {

                              //console.log(jsonData);
                              GlobalVariables.isDebugDevice = jsonData.isDebug;
                              GlobalVariables.message2show_text = jsonData.message2show_text;
                              GlobalVariables.message2show_wait = jsonData.message2show_wait;

                              $ionicLoading.hide();

                              if(err) {

                                  HelperService.showUnrecoverableError(err, false);

                              } else {

                                  if(jsonData.progetti.length == 1) {

                                      //GlobalVariables.application.disableBackButtonToHome = true;
                                      GlobalVariables.application.listaProgetti = jsonData.progetti;
                                      GlobalVariables.application.comingFrom = "START";
                                      GlobalVariables.application.poi = jsonData.poi;
                                      $state.go('home');

                                      /*
                                      GlobalVariables.application.currentTestata = jsonData.progetti[0];
                                      GlobalVariables.application.applicationUrl = jsonData.progetti[0].socketUrl;
                                      $state.go('testata_detail');
                                      */
                                  } else {

                                      GlobalVariables.application.listaProgetti = jsonData.progetti;
                                      GlobalVariables.application.poi = jsonData.poi;
                                      GlobalVariables.application.comingFrom = "START";
                                      $state.go('home');


                                  }

                                  WelcomeScreenHelper.closeSplashScreen();


                                  if(window.cordova && LazyLoader.appConfiguration.showUsersGuideAtStartup == true) {
                                      setTimeout(function() {
                                          GuideServiceHelper.showUsersGuide();
                                      }, 1000);
                                  }


                              }

                          }

                      });

                  }

              }

          });

    }

    var preStart = function(check) {

//      alert("PreStart");
      
      
      window.plugins.uniqueDeviceID.get(function(code) {
        GlobalVariables.deviceUUID = code;
      }, function() {
        GlobalVariables.deviceUUID = 'fallback-' + device.uuid;
      });

      try {
        cordova.getAppVersion.getVersionNumber().then(function(version) {
          GlobalVariables.version = version;
        });  
      } catch(err) {
        GlobalVariables.version = window.device.version;
      }
      
      try {
        cordova.getAppVersion.getVersionCode().then(function(build) {
          GlobalVariables.build = build;
        });
      } catch(err) {
        GlobalVariables.build = '0';
      }

      try {
        cordova.getAppVersion.getPackageName().then(function(packageName) {
          GlobalVariables.packageName = packageName;
        });
      } catch(err) {
        GlobalVariables.packageName = 'com.mediasoft.lve.clex';
      }

      GlobalVariables.platform = window.device.platform;
      GlobalVariables.manufacturer = window.device.manufacturer;

      // ################################################################## //
      // la finestra di welcome viene mostrata solo DOPO il LazyLoader init //
      // ################################################################## //
      
        WelcomeScreenHelper.showSpashScreen({
          onClose: function() {
              doStart();
          },
          showMessage: check
        });


    }    
    ////////////////
    // real start //
    ////////////////

    if(GlobalVariables.application.welcomeScreenHasBeenShown == false) {

        GlobalVariables.application.welcomeScreenHasBeenShown = true;

        if(window.cordova) {

            // ##################### //
            // da dispositivo mobile //
            // ##################### //
            document.addEventListener('deviceready', function() {

                //controllo se è android e non ha dato ancora il permesso per l'external storage
                console.log("controllo preventivo permessi scrittura");
//                alert("controllo preventivo permessi scrittura");
                if (ionic.Platform.isAndroid())
                {
                  console.log("E' android");
                  cordova.plugins.permissions.checkPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                    if ( status.hasPermission ) {
                      console.log("WRITE_EXTERNAL_STORAGE Yes :D ");
//                      alert("WRITE_EXTERNAL_STORAGE Yes :D ");
                      preStart(false);
                    }
                    else {
                      console.warn("WRITE_EXTERNAL_STORAGE No :( ");
//                      alert("WRITE_EXTERNAL_STORAGE No :( ");
                      preStart(true);
                      
                      /*
                      HelperService.showBlockingGenericMessage("Gentile Utente,<br>l'App le richiederà il permesso di accedere alla memoria del dispositivo (o alla SD) per poter scrivere dati.<br>La preghiamo di accettare la richiesta in quanto l'app necessita di scrivere sul suo device i dati e i contenuti necessari al suo funzionamento.<br>Grazie Mille.", "Continua", function(){
                        //alert("cazzo");
                        preStart();
                      });
*/
                       
                    }
                  });
                }
                else
                {
                  console.log("NON E' android");
                  preStart(false);
                }
                
                /*
                window.plugins.uniqueDeviceID.get(function(code) {
                  GlobalVariables.deviceUUID = code;
                }, function() {
                  GlobalVariables.deviceUUID = 'fallback-' + device.uuid;
                });

                try {
                  cordova.getAppVersion.getVersionNumber().then(function(version) {
                    GlobalVariables.version = version;
                  });  
                } catch(err) {
                  GlobalVariables.version = window.device.version;
                }
                
                try {
                  cordova.getAppVersion.getVersionCode().then(function(build) {
                    GlobalVariables.build = build;
                  });
                } catch(err) {
                  GlobalVariables.build = '0';
                }
                
                GlobalVariables.platform = window.device.platform;
                GlobalVariables.manufacturer = window.device.manufacturer;

                // ################################################################## //
                // la finestra di welcome viene mostrata solo DOPO il LazyLoader init //
                // ################################################################## //
                
                //controllo se è android e non ha dato ancora il permesso per l'external storage
                console.log("controllo preventivo permessi scrittura");
                if (ionic.Platform.isAndroid())
                {
                  console.log("E' android");
                  cordova.plugins.permissions.hasPermission(cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                    if ( status.hasPermission ) {
                      console.log("WRITE_EXTERNAL_STORAGE Yes :D ");
                      WelcomeScreenHelper.showSpashScreen({
                        onClose: function() {
                            doStart();
                        }
                      });
                    }
                    else {
                      console.warn("WRITE_EXTERNAL_STORAGE No :( ");
                      HelperService.showBlockingGenericMessage("Gentile Utente,<br>l'App le richiederà il permesso di accedere alla memoria del dispositivo (o alla SD) per poter scrivere dati.<br>La preghiamo di accettare la richiesta in quanto l'app necessita di scrivere sul suo device i dati e i contenuti necessari al suo funzionamento.<br>Grazie Mille.", "Continua", function(){
                        WelcomeScreenHelper.showSpashScreen({
                            onClose: function() {
                                doStart();
                            }
                        });
                      }); 
                    }
                  });
                }
                else
                {
                  console.log("NON E' android");
                  WelcomeScreenHelper.showSpashScreen({
                    onClose: function() {
                        doStart();
                    }
                  });

                }

                */
                /*
                WelcomeScreenHelper.showSpashScreen({
                    onClose: function() {
                        doStart();
                    }
                });
*/

            }, false);

        } else {

          GlobalVariables.deviceUUID = 'browser-' + ionic.Platform.version();

          
          // ########## //
          // da browser //
          // ########## //
          //HelperService.showBlockingGenericMessage("Gentile Utente,<br>l'App le richiederà il permesso di accedere alla memoria del dispositivo (o alla SD) per poter scrivere dati.<br>La preghiamo di accettare la richiesta in quanto l'app necessita di scrivere sul suo device i dati e i contenuti necessari al suo funzionamento.<br>Grazie Mille.", "Continua", function(){
            //alert("dentro");
            WelcomeScreenHelper.showSpashScreen({
                onClose: function() {
                    doStart();
                },
                showMessage: false
            });

          //}); 
          /*
          WelcomeScreenHelper.showSpashScreen({
              onClose: function() {
                  doStart();
              }
          });
*/
        }

    }

})
