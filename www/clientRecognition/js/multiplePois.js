var World = {
	loaded: false,
	rotating: false,

	// different POI-Marker assets
	markerDrawable_idle: null,
	markerDrawable_selected: null,

	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {

		PoiRadar.show();
		$('#radarContainer').unbind('click');
		$("#radarContainer").click(PoiRadar.clickedRadar);


		var numeroMarker=markerList.length;
		for (var i=0;i<numeroMarker;i++) markerList[i].markerObject.destroy();
		
		markerList = [];	

		var dimensioneIndicatoreStandard=0.5;
		
		var numPoi=0;
		for (i=0;i<oggetto.target.length;i++)
		{

			var tipoIndicatore=oggetto.target[i].tipoIndicatore;
			var coloreIndicatore=oggetto.target[i].coloreIndicatore;

			if ((coloreIndicatore!="rosso") && (coloreIndicatore!="bianco") && (coloreIndicatore!="nero") && (coloreIndicatore!="giallo") && (coloreIndicatore!="blu") && (coloreIndicatore!="verde")) coloreIndicatore="rosso";
			if ((tipoIndicatore!="0") && (tipoIndicatore!="1") && (tipoIndicatore!="2") && (tipoIndicatore!="3") && (tipoIndicatore!="4") && (tipoIndicatore!="5") && (tipoIndicatore!="6") && (tipoIndicatore!="7")) tipoIndicatore="0";

			var immagineIndicatoreStandard="assets/"+"indicatore_"+tipoIndicatore+"_"+coloreIndicatore+".png";

			var dimensioneIndicatore=dimensioneIndicatoreStandard;
			var dimTemp = parseInt(oggetto.target[i].dimensione) || 0;
			if (dimTemp != 0) dimensioneIndicatore = dimTemp/100;

			var immagineIndicatore=immagineIndicatoreStandard;
			
			if (oggetto.target[i].hsc_tipo)
			{
				//console.log("E' un hotspot custom nuovo stile, di tipo: " + oggetto.target[i].punti[t].hsc_tipo);
				if (oggetto.target[i].hsc_tipo==0) //immagine
				{
					immagineIndicatore = contenutiPath + oggetto.target[i].hsc_path;
				}
			}				

			var singlePoi = {
				"id": oggetto.target[i].id,
				"latitude": parseFloat(oggetto.target[i].lat),
				"longitude": parseFloat(oggetto.target[i].lon),
				"altitude": parseFloat(oggetto.target[i].alt),
				"realAlt": parseFloat(oggetto.target[i].realAlt),
				"label": oggetto.target[i].label,
				"labelPosition": oggetto.target[i].labelPosition,
				"immagineIndicatore": immagineIndicatore,
				"dimensioneIndicatore": dimensioneIndicatore,
				"distanza": parseFloat(oggetto.target[i].dist),
				"realDist": parseFloat(oggetto.target[i].realDist),
				"labelAddizionale": parseInt(oggetto.target[i].labelAddizionale)
			};

			markerList.push(new Marker(singlePoi));

			distanza=parseFloat(oggetto.target[i].dist)*1000;
			if (distanza<=AR.context.scene.cullingDistance) numPoi++;
		}

		document.getElementById('loadingMessage').innerHTML = "";
		$("#badgeUtenti p").html(numPoi);	
		poiVisibili=numPoi
		poiCaricati=oggetto.target.length;
	}


};


var markerList= new Array();
var position = new Object();
var firstGPSupdate=true;
var iconSelected="";
var poiCaricati=0;
var poiVisibili=0;
var distanzaDefault=-1; //verrà sovrascitta la prima volta che la prende
AR.context.scene.cullingDistance=2000;
var arrayCatergorieScelte= new Array("-1");
var idCliente=16;

var baseUrl = "http://192.170.5.25:8888/liveEngine_webapp";
var contenutiPath="";
var dynClassesPath="";
var contenutiUrl = "";

var itemRilevato="";
var linguaScelta="it";
var livelloAttuale=1;
var livelloPadre="";

var	oggetto = new Array();
var	oggettoTemp = new Array();
var	arrayOverlay = new Array();
var	arrayImages2Load = new Array();
var	arrayOverlayGlobale = new Array(); //contiene n arrayOverlay, uno per ogni immagine da riconoscere nel tracker....
var	arrayIndiciIndicatori = new Array(); //contiene gli indici degli indicatori principali nel'arrayOverlay
var	arrayIndiciIndicatoriGlobale = new Array(); //contiene n arrayIndiciIndicatori, uno per ogni immagine da riconoscere nel tracker....
var	indiceIndicatoreCorrente = -1;
var	arrayCorrispondenzeNomi = new Array(); //contiene n nomi, che mi indicano a quale oggetto dell'array globale mi riferisco
var	arrayTrackable = new Array();
var rotationAnimation;
var arrayAnim = new Array();

var arrayIndiciOverlay_IndiciOggetto = new Array(); //questo array mi mette in relazione l'indice dell'oggetto overlay con l'ondice in oggetto (la variabile)

var videoIsPlaying=false;
var actualVideo=null;

var aspectRationPlayerYT;
var deviceId="";
var swiper=null;
var oggettoAttesaConnessione;
var fullscreenVideo=true;
var fullscreenSwiper=true;
var arrayImagesSwiper=null;

var rotationAnimation;

var dynamicPosClass;
var arrayDynamicClasses = new Array();
var actualClass=null;
var showCommentsDisclaimer;
var commentsNickname;
var stopBarAnim=false;
var paginaRilevata=false;
var contenutoScelto=false;
var language="it";

function sliderPoiChanged()
{
	var range=parseFloat($("#sliderPoi").val());
	console.log(range);
	if (range==0) AR.context.scene.cullingDistance=1;
	else AR.context.scene.cullingDistance=range*1000;
	// quanti sono in relatà quelli visuallizzati? vedo le distanze
	var numPoi=0;
	if (oggetto!= null)
	{
		for (var i=0;i<oggetto.target.length;i++)
		{
			distanza=parseFloat(oggetto.target[i].dist)*1000;
			if (distanza<=AR.context.scene.cullingDistance) numPoi++;
		}
		document.getElementById('loadingMessage').innerHTML = "";
		$("#badgeUtenti p").html(numPoi);	
		poiVisibili=numPoi
	}

}



console.log("sono dentro la RA");

locationChanged = function(latitude, longitude, altitude, accuracy){
  //now, add custom functionality to build the AR scene based on the location
  //console.log("onLocationChanged: " + latitude + " - " + longitude + " - " + altitude + " - " + accuracy);
  //dobbiamo vedere se si è spostato abbastanza dall'ultima posizione conosciuta
  //se si, aggiorno la posizione e ricarico i poi
  //altrimenti ignoro

  //la prima volta che lo ricevo, comunque aggiorno

  var distanza=getDistanceFromLatLonInKm(latitude,longitude,position.latitude,position.longitude)*1000;
  //console.log("Distanza: " + distanza);
  if ((distanza>100) || (firstGPSupdate))
  {
	firstGPSupdate=false;
	console.log("Distanza: " + distanza + " - cambio coordinate - " + latitude + " - "+ longitude + " - "+ altitude);
	
	position.latitude=latitude;
	position.longitude=longitude;
	position.altitude=altitude; 

	//$("#precisione").html(accuracy);
	if (accuracy==1)
	{
		$("#gpsIcon").attr("src","assets/gps1.png").addClass( "blink_me" );
	}
	else if (accuracy==2)
	{
		$("#gpsIcon").attr("src","assets/gps2.png").addClass( "blink_me" );
	}
	else if (accuracy==3)
	{
		$("#gpsIcon").attr("src","assets/gps3.png").removeClass( "blink_me" );
	}
	else
	{
		$("#gpsIcon").attr("src","assets/gps0.png").addClass( "blink_me" );
	}

	/*
	if ((accuracy==1) || (accuracy==2) || (accuracy==3)) $("#gpsIcon").attr("src","assets/gps"+accuracy+".png");
	else $("#gpsIcon").attr("src","assets/gps0.png");
	*/

	// e ricarico i poi
	$("#loadingMessage").html(getLabel("ATTENDERE","Attendere..."));
	loadJson();

  }


}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}


function passData(localPath, localDynamicARPath, idPerc, idCl, baseUrl, localDeviceId, loc_showCommentsDisclaimer, loc_commentsNickname,startLat, startLon, startAlt, systemLanguage)
{
	console.log("v 1.0 - passData: "+localPath+" - "+idPerc+ " - " + idCl + " - " + baseUrl + " - " + localDeviceId + " - " + loc_showCommentsDisclaimer + " - " + loc_commentsNickname + " - " + startLat + " - " + startLon + " - " + startAlt + " - " + systemLanguage);
	//alert("v 1.0 - passData: "+localPath+" - "+idPrj+ " - " + idCl + " - " + baseUrl + " - " + deviceId);

	contenutiPath = localPath + "/";
	dynClassesPath = localDynamicARPath + "/";
	idPercorso = idPerc;
	idCliente = idCl;
	contenutiUrl = baseUrl;
	deviceId=localDeviceId
	showCommentsDisclaimer=loc_showCommentsDisclaimer;
	commentsNickname=loc_commentsNickname
	language=systemLanguage;

	position.latitude=startLat;
	position.longitude=startLon;
	position.altitude=startAlt;

	resizeBasedOnResolution(RA_POI);

	//AR.context.scene.maxScalingDistance=500000; //dopo 5 km gli oggetti non si rimpiccioliscono ulteriormente

	AR.context.scene.scalingFactor=1;
	//$("#messaggioIniziale").show();
	//$("#barraScansione").show();

	/*
	setTimeout(function() {
		$("#messaggioIniziale").hide();
	}, 5000);
*/

	loadJsonLabels(localPath+"/configuration.json");
	$("#backButton").show();

/*
	AR.context.scene.cullingDistance=1;
	console.log(AR.context.scene.cullingDistance);	

	setInterval(function() {
		
		AR.context.scene.cullingDistance=AR.context.scene.cullingDistance+50;
		console.log(AR.context.scene.cullingDistance);	
		// quanti sono in relatà quelli visuallizzati? vedo le distanze
		var numPoi=0;
		if (oggetto!= null)
		{
			
			for (var i=0;i<oggetto.target.length;i++)
			{
				distanza=parseFloat(oggetto.target[i].dist)*1000;
				if (distanza<=AR.context.scene.cullingDistance) numPoi++;
			}
			document.getElementById('loadingMessage').innerHTML = numPoi + " Poi";
		}
	}, 2000);
*/
}

$( window ).resize(function() {
	resizeBasedOnResolution(RA_POI);
});


function loadJsonLabels(myUrl)
{

	oggetto = new Object();
	//console.log("carico il file: " + myUrl);


	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{
			console.log("file json caricato");
			labelsObj=json.labels;

			//traduco le labels
			//$("#labelChiudi").html(getLabel("BTN_RITORNA","Ritorna"));
			$("#loadingMessage").html(getLabel("ATTENDERE","Attendere..."));
			$("#sliderPoiContainer p").html(getLabel("MAX_DISTANCE_POI","Distanza massima di visualizzazione dei POI, in Km"));
			$("#noSignalText").html(getLabel("NESSUN_SEGNALE_POI","Nessun<br>Segnale"));
			$("#minSignalText").html(getLabel("PRECISIONE_MINIMA_POI","Nessun<br>Segnale"));
			$("#medSignalText").html(getLabel("PRECISIONE_MEDIA_POI","Nessun<br>Segnale"));
			$("#maxSignalText").html(getLabel("PRECISIONE_MASSIMA_POI","Nessun<br>Segnale"));
			$("#filterPoiContainer p").html(getLabel("FILTRA_POI_CATEGORIE","Filtra i Punti di Interesse per categorie"));

			loadJson();

			AR.context.onLocationChanged = locationChanged;


		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );

		});
}

function loadJson()
{
	var myUrl = contenutiUrl+"/liveDataPoi.php";

	oggetto = new Object();
	//console.log("carico il file: " + myUrl);


	console.log("loadJson: " + myUrl);
	

	var objParam = new Object();
	objParam.idCliente = idCliente;
	objParam.idPercorso = idPercorso;
	objParam.latitude = position.latitude;
	objParam.longitude = position.longitude;
	objParam.categorie = arrayCatergorieScelte.join();
	objParam.lang = "it";

	console.log(JSON.stringify(objParam));

	$.ajax({
		url: myUrl,
		dataType: 'json',
		type:"GET",
		data:objParam
		})
		.done(function( json )
		{
		
			oggetto.target=json.poi;
			oggetto.categorie=json.categorie;
			oggetto.max_distance=(Number(json.max_distance));
			oggetto.default_distance=(Number(json.default_distance));
			//alert(JSON.stringify(oggetto));
		
			//alert(distanzaDefault);
			if (distanzaDefault==-1)
			{
				distanzaDefault=oggetto.default_distance*1000;	
				//alert("distanzaDefault: " + distanzaDefault);
				AR.context.scene.cullingDistance=distanzaDefault;
				$("#sliderPoi").attr("value",oggetto.default_distance);
				//alert("slider value: " + oggetto.default_distance);
				
			}
			else
			{
				//alert("sono nell'else");
				//lascio quella che ci sta, modificata o meno dall'utente
			}

			$("#sliderPoi").attr("max",oggetto.max_distance);
			$("#sliderPoi").slider("refresh");

			var step=500; //500 km si step dello slider, poi rifiniamo
			 //500 km si step dello slider, poi rifiniamo
			
			if (oggetto.max_distance<1000) step=100;
			if (oggetto.max_distance<500) step=50;
			if (oggetto.max_distance<100) step=10;
			if (oggetto.max_distance<50) step=5;
			if (oggetto.max_distance<20) step=1;
			if (oggetto.max_distance<10) step=0.5;
			if (oggetto.max_distance<5) step=0.1;

			$("#sliderPoi").attr("step",step);
			console.log("Step:" + step);

			//console.log(JSON.stringify(oggetto));

			//mi salvo l'altitudine e la distanza reali, in caso vengano modificate dalla procedura di cluster e poi mostrate quelle false....
			for (i=0;i<oggetto.target.length;i++)
			{
				oggetto.target[i].realAlt=oggetto.target[i].alt;
				oggetto.target[i].realDist=oggetto.target[i].dist;
			}


			clusterPoi(5,4); //clusterizzo i poi che hanno scarto tra di loro di 5 gradi, e nel caso si sovrappongano, l'angolo minimo tra di loro sarà 5 gradi

			//creo il contentManager che mi gestirà i contenuti visualizzati sul device
			contentManager = new ContentManager(contenutiUrl,contenutiPath,"#aaaaaa", RA_POI);

			console.log(JSON.stringify(oggetto));
			console.log("faccio partire Wiki....");
			//alert("init1");
			World.init();


		})
		.fail(function( jqXHR, textStatus ) {
	  		console.log( "Request failed: " + textStatus );

		});

}

function clusterPoi(angolo, angoloV)
{
	var cluster=new Array();
	//ogni elemento dell'array è un array di poi vicini tra di loro 

	//prima fase, calcolo i gradi dei poi rispetto all'utente
	for (i=0;i<oggetto.target.length;i++)
	{
		var bearing=calcolaBearing(position.latitude,position.longitude,parseFloat(oggetto.target[i].lat),parseFloat(oggetto.target[i].lon));
		oggetto.target[i].bearing=bearing;
	}
	//console.log("ho aggiornato il bearing degli oggetti");

	for (i=0;i<oggetto.target.length;i++)
	{
		//lo metto in un cluster, potrà rimanere singolo o associatoi ad altri
		//devo vedere se questo poi è stato già inserito in un clusetr... se si, prendo quello, altrimen ti ne creo uno nuovo
		//console.log("poi: " + i);
		var oggettoCluster;
		var clusterCheck=findCluster(cluster, oggetto.target[i]);
		if (clusterCheck==null)
		{
			//console.log("oggetto non trovato nel cluster: ");
			oggettoCluster = new Array();
			oggettoCluster.push(oggetto.target[i]);
		}
		else
		{
			//console.log("oggetto trovato nel cluster: " + JSON.stringify(cluster[clusterCheck]));
			oggettoCluster = cluster[clusterCheck];
		}
		// per ogni poi vedo se ce ne sono vicini da associare
		//console.log("bearing poi: " + oggetto.target[i].bearing);
		for (t=i+1;t<oggetto.target.length;t++)
		{
			//console.log("bearing poi "+t+": " + oggetto.target[t].bearing);
			if ((oggetto.target[t].bearing<(oggetto.target[i].bearing+angolo)) && (oggetto.target[t].bearing>(oggetto.target[i].bearing-angolo)))
			{
				oggettoCluster.push(oggetto.target[t]);
				//console.log("sono vicini, lo associo...");
			}
		}
		if (clusterCheck==null) cluster.push(oggettoCluster);		
		else cluster[clusterCheck]=oggettoCluster;
		//console.log("finito poi "+i+" - oggettoCluster: " + JSON.stringify(oggettoCluster));
	}
	//console.log("finiti tutti i poi");

	//per ogni cluster che ha più di un elemento, aggiusto le altezze, se nel caso....
	oggetto.target=[]; //lo azzero
	for (i=0;i<cluster.length;i++)
	{
		//console.log("cluster "+i+": " + JSON.stringify(cluster[i]));
		var oggettoCluster = cluster[i];
		if (oggettoCluster.length>1)
		{
			//lo processo, ci sono più di un poi

			//li ordino, dal più basso al più alto
			oggettoCluster.sort(function(a, b){
			    return a.alt - b.alt;
			});
			var ultimoAngoloAltezza=-90; //per partire
			for (var t=0;t<oggettoCluster.length;t++)
			{
				var altezzaPoi=parseInt(oggettoCluster[t].alt); 
				var distanzaPoi=parseInt(oggettoCluster[t].dist*1000);
				var altezzaRelativa=altezzaPoi-parseInt(position.altitude);
				//console.log("altezzaPoi: " + altezzaPoi);
				//console.log("distanzaPoi: " + distanzaPoi);
				//console.log("altezzaRelativa: " + altezzaRelativa);

				angoloAltezza = (Math.atan2(altezzaRelativa, distanzaPoi))* 180 / Math.PI;
				//console.log("ultimoAngoloAltezza: " + ultimoAngoloAltezza);
				//console.log("angoloAltezza: " + angoloAltezza);
				if (angoloAltezza>ultimoAngoloAltezza+angoloV)
				{
					//console.log("questo angolo va bene");
					//tutto ok, questo angolo va bene
					ultimoAngoloAltezza=angoloAltezza;
				}
				else
				{
					//non va bene... metto il valore minimo di altezza che mi permette un angolo soddisfacente
					//console.log("questo angolo NON va bene");
					var angoloMinimo=ultimoAngoloAltezza+angoloV;
					var nuovaAltezzaRelativa=distanzaPoi*Math.tan(deg2rad(angoloMinimo));
					altezzaPoi=nuovaAltezzaRelativa+parseInt(position.altitude);
					//console.log("angoloMinimo: " + angoloMinimo);
					//console.log("nuovaAltezzaRelativa: " + nuovaAltezzaRelativa);
					//console.log("altezzaPoi: " + altezzaPoi);
					oggettoCluster[t].alt=altezzaPoi;
					ultimoAngoloAltezza=angoloMinimo;
				}
				
				//oggettoCluster[t].label="c"+i+" - "+oggettoCluster[t].label;
				oggetto.target.push(oggettoCluster[t]);
			}
			//console.log("cluster "+i+" riordinato: " + JSON.stringify(oggettoCluster));

		}
		else
		{
			oggetto.target.push(oggettoCluster[0]);
		}
	}	

}

function findCluster(clusterArray, oggetto2Find)
{
	var ret=null;

	for (var i=0;i<clusterArray.length;i++)
	{
		var oggettoArray=clusterArray[i];
		for (var t=0;t<oggettoArray.length;t++)
		{
			if (oggettoArray[t].id==oggetto2Find.id) ret=i;
		}
	}

	return ret;
}


function calcolaBearing(latUser,lonUser,latPoi,lonPoi)
{

	var dLon = deg2rad(lonPoi-lonUser); 

	var y = Math.sin(dLon) * Math.cos(deg2rad(latPoi));
	var x = Math.cos(deg2rad(latUser))*Math.sin(deg2rad(latPoi)) - Math.sin(deg2rad(latUser))*Math.cos(deg2rad(latPoi))*Math.cos(dLon);
	var bearingRadians = Math.atan2(y, x);
    var bearingDegrees = bearingRadians * (180.0 / Math.PI); // convert to degrees
    bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
	//console.log("Bearing calcolato tra user: " + latUser + " - " + lonUser + " e il poi: " + latPoi + " - " + lonPoi + " = " + bearingDegrees);

    return parseInt(bearingDegrees);
}

function navigaVerso(poiId)
{
	console.log("navigaVerso: " + poiId);
	var lat;
	var lon;
	for (var i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].id==poiId)
		{
			navigaVersoPosizione(oggetto.target[i].lat, oggetto.target[i].lon);
		}
	}
	
}

function showContenuto(poiId)
{
	console.log("showContenuto: " + poiId);

	var label;
	var distanza;
	var distanzaStr="";
	var config=[];
	
	for (var i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].id==poiId)
		{
			//console.log("Ho trovato l'elemento:");
			//console.log(oggetto.target[i]);
			//console.log(JSON.stringify(oggetto.target[i]));
			label=oggetto.target[i].label;
			config=JSON.parse(oggetto.target[i].config);
			distanza=parseFloat(oggetto.target[i].dist);
			if (distanza<1) distanzaStr=parseInt(distanza*1000)+" "+getLabel("METRI","m");
			else distanzaStr=distanza.toFixed(1)+" Km";
		}
	}

	if (config==null) config=[];

	var descrizione=label+"<br>"+getLabel("DISTANZA","Dist")+": "+ distanzaStr;

	creaPopupSceltaContenuto(RA_POI, config, descrizione, null, poiId);

	/*
	$("#infoText").html(label+"<br>"+getLabel("DISTANZA","Dist")+": "+ distanzaStr);

	//devo mettere i pulsanti, a seconda dei contenuti che ha scelto
	//il primo è standard, navigazione
	var html="";
	html+="<div class='buttonPoi ui-btn ui-shadow' style='float:left;' onClick='navigaVerso("+poiId+");'><i class='menuIconAwesome fa fa-map-o' aria-hidden='true' style='margin-right: 20px;'></i>"+getLabel("NAVIGA_VERSO","Naviga verso")+"....</div>";
	//aggiungo quelli propri del config
	if (config!=null)
	{
		for (var i=0;i<config.length;i++)
		{
			var label="";
			if (config[i].tipo==1) //video
			{
				if (typeof config[i].label != 'undefined')
				{
					label=config[i].label;
				}
				else
				{
					label="Video";
				}
				html+="<div class='buttonPoi ui-btn ui-shadow' style='float:left;' onClick='mostraContenutoPoi("+poiId+","+i+");'><i class='menuIconAwesome fa fa-video-camera' aria-hidden='true' style='margin-right: 20px;'></i>"+label+"</div>";
			}
			else if (config[i].tipo==6) //link esterno
			{
				if (typeof config[i].label != 'undefined')
				{
					label=config[i].label;
				}
				else
				{
					label="Link";
				}
				html+="<div class='buttonPoi ui-btn ui-shadow' style='float:left;' onClick='mostraContenutoPoi("+poiId+","+i+");'><i class='menuIconAwesome fa fa-globe' aria-hidden='true' style='margin-right: 20px;'></i>"+label+"</div>";
			}
		}
	}	
	html+="<div style='clear:both;'></div>";

	$("#infoPulsanti").html(html);
	$("#infoContainer").show();
	resizeBasedOnResolution(RA_POI);


	return;
	*/
}

function getConfigByPoiId(poiId,contenutoId)
{
	var config;
	for (var i=0;i<oggetto.target.length;i++)
	{
		if (oggetto.target[i].id==poiId) config=JSON.parse(oggetto.target[i].config);
	}
	return config[contenutoId];
}

function mostraContenutoPoi(poiId,contenutoId)
{
	console.log("sono in mostraContenutoPoi, con poiId: "+poiId+ " e contenutoId: " + contenutoId);
	$("#infoPulsanti").html("");
	$("#infoContainer").hide();

	var oggettoPunto=getConfigByPoiId(poiId,contenutoId);

	//per alcuni conteuti è necessaria la verifica della connessione
	//in quel caso, mi memorizzo i dati che verranno usati al ritorno della funzione
	switch (parseInt(oggettoPunto.tipo)) {
	    case 1: //video
	    case 2: //slideshow
	    case 5: //elemento scaricabile
	    case 1001: //elemento collaborativo - votazione, gradimento
	    case 1002: //elemento collaborativo - commento
	    case 1003: //elemento collaborativo - condivisione
			oggettoAttesaConnessione = new Object();
			oggettoAttesaConnessione.tipo = oggettoPunto.tipo;
			oggettoAttesaConnessione.poiId = poiId;
			oggettoAttesaConnessione.contenutoId = contenutoId;
			$("#loader").show();
			requestConnectionStatus();
	        break; 
	    case 3: //testo
			oggettoTesto=JSON.parse(oggettoPunto.config);
			contentManager.setTesto(oggettoTesto);
	        break; 
	    case 4: //html zip
			//per questo tipo di contenuto devo prima scaricare il file, scompattarlo e dopo eseguirlo...
			$("#loader").show();
			scaricaHtmlZip(oggettoPunto.config);
	        break;
	    case 6: //link esterno
			oggettoLink=JSON.parse(oggettoPunto.config);
			openExternalLink(oggettoLink.url);
	        break; 
	    default: 
	        //default, mai eseguito;
	}	


/*
	if (config[contenutoId].tipo==1) //video
	{
		console.log("è un video");
		//alert("video");

		oggettoAttesaConnessione = new Object();
		oggettoAttesaConnessione.tipo = 1;
		oggettoAttesaConnessione.poiId = poiId;
		oggettoAttesaConnessione.contenutoId = contenutoId;

		$("#loader").show();

		document.location = 'architectsdk://checkConnessione';

	}
	else if (config[contenutoId].tipo==2) //slideshow
	{
		console.log("è uno slideshow");

		oggettoAttesaConnessione = new Object();
		oggettoAttesaConnessione.tipo = 2;
		oggettoAttesaConnessione.immagine = immagine;
		oggettoAttesaConnessione.punto = punto;

		$("#loader").show();

		document.location = 'architectsdk://checkConnessione';

	}
	else if (config[contenutoId].tipo==3) //testo
	{
		console.log("è un testo");
		var oggettoTesto=JSON.parse(oggetto.target[immagine].punti[punto].config)
 		creaTesto(oggettoTesto.text);

	}
	else if (config[contenutoId].tipo==4) //html zip
	{
		console.log("è uno zip");
		//per questo tipo di contenuto devo prima scaricare il file, scompattarlo e dopo eseguirlo...
		$("#loader").show();
		scaricaHtmlZip(oggetto.target[immagine].punti[punto].config);
	}
	else if (config[contenutoId].tipo==5) //elemento scaricabile
	{
		console.log("è un Elemento scaricabile");


		oggettoAttesaConnessione = new Object();
		oggettoAttesaConnessione.tipo = 5;
		oggettoAttesaConnessione.immagine = immagine;
		oggettoAttesaConnessione.punto = punto;

		$("#loader").show();

		document.location = 'architectsdk://checkConnessione';
	}
	else if (config[contenutoId].tipo==6) //link esterno
	{
		console.log("è un link esterno");
		//alert("è un link esterno");

		var oggettoLink=JSON.parse(config[contenutoId].config)
		//alert(JSON.stringify(oggettoLink));
		document.location = 'architectsdk://linkEsterno_'+oggettoLink.url;
	}
*/
}


function checkConnessione(status)
{
	$("#loader").hide();
	console.log("tornato da check connessione, con status: " + status);

	if (status==true) //connessione presente
	{
		if (oggettoAttesaConnessione.tipo==1) //video
		{
			var poiId=oggettoAttesaConnessione.poiId;
			var contenutoId=oggettoAttesaConnessione.contenutoId;
			var oggettoPunto=getConfigByPoiId(poiId,contenutoId);
			var oggettoVideo=JSON.parse(oggettoPunto.config);
			//per dare il controllo al video
			$(".ui-page-active").css("pointer-events","auto");	
			contentManager.setVideo(oggettoVideo);
		}
		else if (oggettoAttesaConnessione.tipo==2) //slideshow
		{
			var poiId=oggettoAttesaConnessione.poiId;
			var contenutoId=oggettoAttesaConnessione.contenutoId;
			var oggettoPunto=getConfigByPoiId(poiId,contenutoId);
			var arrayOggettoImmagine=JSON.parse(oggettoPunto.config);
			//per dare il controllo allo slideshow
			$(".ui-page-active").css("pointer-events","auto");	
			contentManager.setSlideshow(arrayOggettoImmagine, true, "POI"+oggettoAttesaConnessione.poiId); //connessione presente
		}
		else if (oggettoAttesaConnessione.tipo==5)
		{
			var poiId=oggettoAttesaConnessione.poiId;
			var contenutoId=oggettoAttesaConnessione.contenutoId;
			var oggettoPunto=getConfigByPoiId(poiId,contenutoId);
			var oggettoScaricabile=JSON.parse(oggettoPunto.config);
			//per dare il controllo allo slideshow
			$(".ui-page-active").css("pointer-events","auto");	
			contentManager.setElementoScaricabile(oggettoScaricabile);
		}
		else if (oggettoAttesaConnessione.tipo==1000)
		{
			//per adesso nessun contenuto dinamico
		}
	}
	else
	{
		//alert("Nessuna connessione disponibile");
		//ma per lo slideshow potrei aver cachato i files
		if (oggettoAttesaConnessione.tipo==2)
		{
			//ora devo verificare se qualcosa era stato salvato in precedenza
			var poiId=oggettoAttesaConnessione.poiId;
			var contenutoId=oggettoAttesaConnessione.contenutoId;
			var oggettoPunto=getConfigByPoiId(poiId,contenutoId);
			console.log("vado a existSlideshow, con oggettoAttesaConnessione: " + JSON.stringify(oggettoAttesaConnessione));
			existSlideshow("POI"+oggettoAttesaConnessione.poiId);
			//e aspetto una risposta, non cancellando oggettoAttesaConnessione che mi potrà servire...
		}
		else if (oggettoAttesaConnessione.tipo==1000) //contenuto dinamico.. non faccio niente
		{
			console.log("non posso abilitare gli oggetti dinamici");
		}
		else
		{
			$("#messaggioGenerico p").html("Nessuna connessione disponibile");
			$("#messaggioGenerico").show();

			setTimeout(function() {
				$("#messaggioGenerico").hide();
			}, 4000);
			
			console.log("cancello oggettoAttesaConnessione");
			oggettoAttesaConnessione = null;
			delete oggettoAttesaConnessione;			
		}
	}
}

function checkConnessioneSlideshow(status) //richiamata quando non ci stava connessione e voglio vedere se lo lideshow esiste in locale, salvato in precedenza
{
	console.log("tornato da existSlideshow, con oggettoAttesaConnessione: " + JSON.stringify(oggettoAttesaConnessione));
	if (status==true) //qualche immagine dello slideshow è presente
	{
		var poiId=oggettoAttesaConnessione.poiId;
		var contenutoId=oggettoAttesaConnessione.contenutoId;
		var oggettoPunto=getConfigByPoiId(poiId,contenutoId);
		var arrayOggettoImmagine=JSON.parse(oggettoPunto.config);
		//per dare il controllo allo slideshow
		$(".ui-page-active").css("pointer-events","auto");	
		contentManager.setSlideshow(arrayOggettoImmagine, false, "POI"+oggettoAttesaConnessione.poiId); //connessione non presente
	}
	else
	{
		//alzo definitivamente bandiera bianca
		$("#messaggioGenerico p").html("Nessuna connessione disponibile");
		$("#messaggioGenerico").show();

		setTimeout(function() {
			$("#messaggioGenerico").hide();
		}, 4000);		
	}
	console.log("cancello oggettoAttesaConnessione");
	oggettoAttesaConnessione = null;
	delete oggettoAttesaConnessione;	
}

function back() //quando si chiudono elementi come video o slideshow
{
	//per fare in modo che possa cliccare i poi
	$(".ui-page-active").css("pointer-events","none");	
	backUtils(RA_POI);
}



function applyFilterPoi()
{
	//vedo quali checkbox sono checkati
	arrayCatergorieScelte=[];
	// se è cliccato "tutti", metto solo il -1
	if ($("#cb-1").is(':checked'))
	{
		arrayCatergorieScelte.push("-1");
	}
	else
	{
		$('#filterCheckboxContainer input').each(function(index, currentElem){
		    if ($(currentElem).is(':checked'))
		    {
				arrayCatergorieScelte.push($(currentElem).attr("value"));
		    }
		});
	}
	if(arrayCatergorieScelte.length==0) arrayCatergorieScelte.push("-1");
	console.log(JSON.stringify(arrayCatergorieScelte));
	// e poi dovrei fare la nuova query, filtrando per categorie
	loadJson();
}


function clickOnIcon(type)
{
	var opacity=0.3;
	console.log("cliccato su " + type);
	if (iconSelected=="") //niente selezionato al momento
	{
		$("#badgeUtenti img").css("opacity",opacity);	
		$("#badgeUtenti p").css("opacity",opacity);	
		$("#"+type+"Icon").css("opacity","1");	
		iconSelected=type;
	}
	else //una icona è già selezionata
	{
		//se è l'icona del filtro, devo applicare i filtri, sia che lo stia togliendo, sia che stia passando a un'altra icona
		if (iconSelected=="filter") applyFilterPoi();
		if (iconSelected==type) //è la stessa
		{
			$("#badgeUtenti img").css("opacity","1");	
			$("#badgeUtenti p").css("opacity","1");	
			iconSelected="";
		}
		else //è un'altra
		{
			$("#badgeUtenti img").css("opacity",opacity);	
			$("#badgeUtenti p").css("opacity",opacity);	
			$("#"+type+"Icon").css("opacity","1");	
			iconSelected=type;
		}
	}

	//tolgo tutto
	$("#sliderPoiContainer").hide();
	$("#gpsInfoContainer").hide();
	$("#infoPoiContainer").hide();
	$("#filterPoiContainer").hide();

	if (iconSelected=="gps")
	{
		$("#gpsInfoContainer").show();
	}
	else if (iconSelected=="range")
	{
		$("#sliderPoiContainer").show();
	}
	if (iconSelected=="poi")
	{
		$("#badgeUtenti p").css("opacity","1");	
		$("#infoPoiContainer p").html(poiCaricati + " " + getLabel("POI_DISPONIBILI","Punti di Interesse (POI) disponibili")+ "<br>" + poiVisibili + " " + getLabel("POI_VISUALIZZATI_NEL_RAGGIO","visualizzati in un raggio di") + " " + AR.context.scene.cullingDistance + " " + getLabel("METRI","metri"));
		$("#infoPoiContainer").show();
	}
	if (iconSelected=="filter")
	{
		//svuoto tutto
		$("#filterCheckboxContainer").html("");
		//metto di base quella Tutti
		var selezionato = checkCategoriaSelezionata("-1");

		$('<input />', { type: 'checkbox', id: 'cb-1', value: '-1', checked: selezionato}).appendTo($("#filterCheckboxContainer"));
		$('<label />', { 'for': 'cb-1', text: 'Tutti' }).appendTo($("#filterCheckboxContainer"));

		for (var i=0;i<oggetto.categorie.length;i++)
		{
			selezionato = checkCategoriaSelezionata(oggetto.categorie[i].id);
			$('<input />', { type: 'checkbox', id: 'cb'+oggetto.categorie[i].id, value: oggetto.categorie[i].id , checked: selezionato}).appendTo($("#filterCheckboxContainer"));
			$('<label />', { 'for': 'cb'+oggetto.categorie[i].id, text: oggetto.categorie[i].nome }).appendTo($("#filterCheckboxContainer"));
		}
		$("#filterCheckboxContainer").trigger('create');
		$("#filterPoiContainer").show();
		
	}

}

function checkCategoriaSelezionata(id)
{
	var ret=false;
	for (var i=0;i<arrayCatergorieScelte.length;i++)
	{
		if (arrayCatergorieScelte[i]==id) ret=true;
	}
	return ret;		
}

$(document).on('change', 'input[type="checkbox"]', function(e){
    var id=$(e.currentTarget).attr("value");
    var checked=$(e.currentTarget).is(':checked')
    console.log(id);
    console.log(checked);
    if (id=="-1") //ha cliccato su "tutti"
    {
		$('#filterCheckboxContainer input').each(function(index, currentElem){
		    $(currentElem).prop('checked',checked).checkboxradio('refresh');
		});
    }
    else //ha cliccato su un altro... se è disabilitato, tolgo anche quello su tutti
    {
    	if (!checked)
    	{
    		$('#cb-1').prop('checked',checked).checkboxradio('refresh');
    	}
    }

});	

$(document).ready(function()
{
	//solo funzioni per testare nel browser


/*
	contentManager = new ContentManager("","","#ff0000");
	var arrayImages=new Array();
	arrayImages.push("");
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638560_slide4.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638562_slide3.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638567_slide1.jpg";
	arrayImages.push(img);
	var img = "http://livengine.mediasoftonline.com/contenuti/440_4_1507638568_slide2.jpg";
	arrayImages.push(img);
	fullscreenSwiper=true;
	contentManager.creaSlideshowNew(arrayImages,true);
*/
/*
	$('#filterCheckboxContainer input').click(function() {
	    console.log($(this).attr("id"));
	    console.log($(this).attr("checked"));
	});
*/
	
	/*
	resizeBasedOnResolution(RA_POI);
	var json=JSON.parse('{"poi":[{"id":"2","label":"Prova","lat":"40.176114","lon":"18.169842","alt":"100","labelPosition":"","dimensione":"90","tipoIndicatore":"-1","coloreIndicatore":"verde","rotazione":"0","periodo_rotazione":"0","dist":"0.07940258017508597"},{"id":"1","label":"Chiesa del Carmine","lat":"40.176648","lon":"18.171415","alt":"130","labelPosition":"","dimensione":"60","tipoIndicatore":"-1","coloreIndicatore":"rosso","rotazione":"0","periodo_rotazione":"0","dist":"0.08731188567065959"},{"id":"10","label":"Pub La Staffa","lat":"40.175456","lon":"18.172014","alt":"90","labelPosition":"","dimensione":"10","tipoIndicatore":"-1","coloreIndicatore":"blu","rotazione":"0","periodo_rotazione":"0","dist":"0.19488494073992843"},{"id":"7","label":"Pub Zona Franca","lat":"40.177293","lon":"18.168231","alt":"90","labelPosition":"","dimensione":"10","tipoIndicatore":"-1","coloreIndicatore":"blu","rotazione":"0","periodo_rotazione":"0","dist":"0.1951033322223834"},{"id":"6","label":"Chiesa Santa Caterina Novella","lat":"40.17865","lon":"18.17112","alt":"130","labelPosition":"","dimensione":"60","tipoIndicatore":"-1","coloreIndicatore":"rosso","rotazione":"0","periodo_rotazione":"0","dist":"0.22629715646599946"},{"id":"4","label":"Santa Maria delle Grazie","lat":"40.175673","lon":"18.167789","alt":"130","labelPosition":"","dimensione":"60","tipoIndicatore":"-1","coloreIndicatore":"rosso","rotazione":"0","periodo_rotazione":"0","dist":"0.2483121962567784"},{"id":"5","label":"Chiesa Madre","lat":"40.174216","lon":"18.169763","alt":"130","labelPosition":"","dimensione":"60","tipoIndicatore":"-1","coloreIndicatore":"rosso","rotazione":"0","periodo_rotazione":"0","dist":"0.2805178861298391"},{"id":"9","label":"San Pedro Saloon","lat":"40.173829","lon":"18.169095","alt":"90","labelPosition":"","dimensione":"10","tipoIndicatore":"-1","coloreIndicatore":"blu","rotazione":"0","periodo_rotazione":"0","dist":"0.33690500187678435"},{"id":"8","label":"Ristorante Sonora","lat":"40.177902","lon":"18.165985","alt":"90","labelPosition":"","dimensione":"10","tipoIndicatore":"-1","coloreIndicatore":"blu","rotazione":"0","periodo_rotazione":"0","dist":"0.3975762654268851"},{"id":"3","label":"Basilica di S.Caterina d\'Alessandria","lat":"40.17272","lon":"18.172145","alt":"130","labelPosition":"down","dimensione":"60","tipoIndicatore":"-1","coloreIndicatore":"rosso","rotazione":"0","periodo_rotazione":"0","dist":"0.4662895002410044"}],"categorie":[{"id":"1","nome":"Chiese"},{"id":"2","nome":"Ristorazione"},{"id":"0","nome":"Altri"}]}');

	oggetto.target=json.poi;
	oggetto.categorie=json.categorie;

	showContenuto(1);
	*/

	//$("#backButton").show();
	//getUsersNumber(4);
	/*
	$("body").css("background-color","#000000");
	resizeBasedWindows();
	var arrayImages=new Array();
	arrayImages.push("");
	var img = "http://www.cliccascienze.it/files/isola.jpg";
	arrayImages.push(img);
	img = "https://www.greenme.it/immagini/viaggiare/eco-turismo/isole_cuore/isole_a_forma_di_cuore.jpg";
	arrayImages.push(img);
	
	creaSlideshowNewTEST(arrayImages);
*/

	
	return;



});
