var World = {
	loaded: false,
	rotating: false,


	init: function initFn() {
		this.loaded=false;
		//AR.context.destroyAll();
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {

		this.tracker = new AR.ClientTracker(contenutiPath + wtcName, {
			onLoaded: this.worldLoaded
		});

		arrayOverlay = [];

		var trackable = new AR.ImageTrackable(this.tracker, "*", {
			drawables: {
				cam: arrayOverlay
			},
			onImageRecognized: function onEnterFieldOfVisionFn(a)
            {
				console.log("ho rilevato " + a);
				var nome="";
				
				copertinaRilevata(a);

				//trackable.snapToScreen.enabled = false;
			},
/*			snapToScreen: {
				enabledOnExitFieldOfVision: false,
				snapContainer: document.getElementById('snapContainer')
			},	
*/			onImageLost: function onExitFieldOfVisionFn(a)
            {
				console.log("ho perso la rilevazione di " + a);
				//var cssDivInstructions = " style='display: table-cell;vertical-align: middle; text-align: center; width: 50%; padding-right: 15px;'";
				//document.getElementById('loadingMessage').innerHTML ="<div" + cssDivInstructions + ">Looking for Target.....</div>";

				//trackable.snapToScreen.enabled = false;
			},
		});


	},



	worldLoaded: function worldLoadedFn() {
		console.log("worldLoaded");
		startAnimationBar(RA_COPERTINA);
	}



};


var logEnabled= false;
var language="it";

if (logEnabled==true)
{
	AR.logger.activateDebugMode();
}

console.log("sono qui dentro");

function passData(localPath, wtc, baseUrl,numUtenti,colore, systemLanguage)
{   
	console.log("v 1.0 - passData: "+localPath + " - " + wtc + " - " + baseUrl + " - " + numUtenti + " - " + colore + " - " + systemLanguage);
	//alert("v 1.0 - passData: "+localPath + " - " + wtc + " - " + baseUrl + " - " + numUtenti + " - " + colore);
	//console.log("v 1.0 - passData: "+localPath + " - " + wtc);
	//alert("qui");
	
	contenutiPath = localPath + "/";
	wtcName=wtc;
	contenutiUrl = baseUrl;
	numeroUtenti=numUtenti;
	if (numeroUtenti==0) numeroUtenti=1; //almeno lui
	language=systemLanguage;

	$("#numeroUtenti").html(numeroUtenti);
	

	$("#barraScansione").css("background-color",colore).css("-webkit-box-shadow","0px 0px 30px 2px "+colore).css("-moz-box-shadow","0px 0px 30px 2px "+colore).css("box-shadow","0px 0px 30px 2px "+colore);

	var col1=convertHex(colore,60);
	var col2=convertHex(colore,30);
	var col3=convertHex(colore,0);

	$( "#header" ).css("background","linear-gradient(to bottom, "+col1+", "+col2+"");
	$( "#subHeader" ).css("background","linear-gradient(to bottom, "+col2+", "+col3+"");
	$( "#footer" ).css("background","linear-gradient(to top, "+col1+", "+col3+"");
	
	//quento è grande la finestra?
	console.log("window.height: "+$(window).height());   // returns height of browser viewport
	console.log("document.height: "+$(document).height()); // returns height of HTML document
	console.log("window.width: "+$(window).width());   // returns width of browser viewport
	console.log("document.width: "+$(document).width()); 

	if (logEnabled==true) AR.logger.info("v 1.0 - passData: "+localPath);

	
	$("#messaggioIniziale").show();
	$("#users_container").show();
	//$("#bottoneRitorna").show();


	resizeBasedOnResolution(RA_COPERTINA);

	setTimeout(function() {
		$("#bottoneNonRiesco").show();
	}, 10000);

	loadJson(localPath+"/configuration.json");

}

//var baseUrl = "http://192.170.5.25:8888/liveEngine_webapp";
var contenutiPath="";
//var idProgetto="";
//var isApiRest = false;
//var immaginiVotate= new Array(); //memorizzo le immagini per cui ha già votato, in modo da non farlo votare più (almeno fini a quando non esce e rientra dalla RA)
var numeroUtenti;

//var itemRilevato="";
//var linguaScelta="it";
//var livelloAttuale=1;
//var livelloPadre="";

var	oggetto = new Array();
//var	oggettoTemp = new Array();
var	arrayOverlay = new Array();
//var	arrayOverlayGlobale = new Array(); //contiene n arrayOverlay, uno per ogni immagine da riconoscere nel tracker....
//var	arrayIndiciIndicatori = new Array(); //contiene gli indici degli indicatori principali nel'arrayOverlay
//var	arrayIndiciIndicatoriGlobale = new Array(); //contiene n arrayIndiciIndicatori, uno per ogni immagine da riconoscere nel tracker....
//var	indiceIndicatoreCorrente = -1;
//var	arrayCorrispondenzeNomi = new Array(); //contiene n nomi, che mi indicano a quale oggetto dell'array globale mi riferisco
//var	arrayTrackable = new Array();
//var rotationAnimation;
//var arrayAnim = new Array();

//var arrayIndiciOverlay_IndiciOggetto = new Array(); //questo array mi mette in relazione l'indice dell'oggetto overlay con l'ondice in oggetto (la variabile)

//var videoIsPlaying=false;
//var actualVideo=null;
var labelsObj;
var contenutiUrl;
var stopBarAnim=false;
var idCopertinaRilevata=-1;
var oggettoAttesaConnessione;


$( window ).resize(function() {
	resizeBasedOnResolution(RA_COPERTINA);
});

/*
function resizeBasedOnResolution()
{
	var larghezza=$( window ).width();
	var altezza=$( window ).height();
	
	if (larghezza>altezza)
	{
		//landscape
		var larghDiv="70%";
		var margDiv="15%";
		if (larghezza<600)
		{
			larghDiv="80%";
			margDiv="10%";
		}

		$("#bottoneNonRiesco").css("width",larghDiv).css("margin-left",margDiv).css("margin-top",((altezza-(altezza/2))/4)+"px");
		$("#messaggioIniziale").css("width",larghDiv).css("margin-left",margDiv).css("margin-top",((altezza-(altezza/3))/3)+"px");
	}
	else
	{
		$("#bottoneNonRiesco").css("width","80%").css("margin-left","10%").css("margin-top",((altezza-(altezza/2))/2)+"px");
		$("#messaggioIniziale").css("width","80%").css("margin-left","10%").css("margin-top",((altezza-(altezza/4))/2)+"px");
	}

	var altezzaBadgeUtenti=$("#badgeUtenti")[0].clientHeight+2; //aggiunto il bordo
	$("#users_container p").css("font-size",altezzaBadgeUtenti*0.8+"px");
	$("#users_container").css("margin-left","0px");
	var larghezzaUsersContainer = $("#users_container").width();
	$("#users_container").css("margin-left",(larghezza-larghezzaUsersContainer)/2+"px");
	$("#numeroUtenti").css("font-size",altezzaBadgeUtenti*0.65+"px");

	$("#barraScansione").css("height",altezza+"px");

	if ($("#imageContainer").is(":visible"))
	{
		//metto il contenitore alla grandezza di default
		$("#imageContainer").css("width","90%").css("left","5%");
		// e anche l'immagine interna
		$("#imageContainerImg").css("width","100%");
		$("#imageContainerImg").css("height","initial");

		var altezzaHeader = $("#header").height();
		var altezzaFooter = $("#footerMsg").height();

		var altezzaImmagineMax=(altezza-altezzaHeader-altezzaFooter)*0.5; //altezza massima immagine
		//console.log("altezza massima: " + altezzaImmagineMax);

	    //l'immagine riesce ad entrare nell'altezza massima?
		var altezzaImg=$("#imageContainerImg")[0].height;
		//console.log("altezza effettiva immagine: " + altezzaImg);

		var posizione;
		if (altezzaImg<altezzaImmagineMax)
		{
			//tutto ok, la devo solo centrare
			posizione=(altezza-altezzaHeader-altezzaFooter-altezzaImg)/2 + altezzaHeader;
		}
		else
		{
			//deve comandare l'altezza massima
			$("#imageContainerImg").css("height",altezzaImmagineMax+"px").css("width","initial");
			//vedo la larghezza effettiva
			var larghImg=$("#imageContainerImg")[0].width;
			// e setto il contenitore
			$("#imageContainer").css("width",larghImg+"px").css("left",((larghezza-larghImg)/2)+"px");
			posizione=(altezza-altezzaHeader-altezzaFooter-altezzaImmagineMax)/2 + altezzaHeader;
			altezzaImg=$("#imageContainerImg")[0].height;

		}
		$("#imageContainer").css("top",posizione+"px");

	}
}
*/


function loadJson(myUrl)
{

	oggetto = new Object();
	console.log("carico il file: " + myUrl);

	if (logEnabled==true) AR.logger.info("url: " + myUrl)

	$.ajax({
		url: myUrl,
		dataType: 'json'
		})
		.done(function( json )
		{
			console.log("file json caricato");
			labelsObj=json.labels;
			
			//traduco le labels
			//$("#bottoneRitorna").html(getLabel("BTN_RITORNA","Ritorna"));
			$("#bottoneNonRiesco p").html(getLabel("BTN_NON_RILEVO_COPERTINA","Non riesco a rilevare la copertina"));
			$("#testoInquadraCopertina").html(getLabel("INQUADRA_COPERTINA","Inquadra la copertina della tua rivista"));
			if (numeroUtenti==1) $("#users_container p").html(getLabel("UTENTE_IN_LINEA","Utente in linea con te"));
			else $("#users_container p").html(getLabel("UTENTI_IN_LINEA","Utenti in linea con te"));
			resizeBasedOnResolution(RA_COPERTINA);

			World.init();
		})
		.fail(function( jqXHR, textStatus ) {
	  		if (logEnabled==true) AR.logger.info("Request failed: " + textStatus)
	  		console.log( "Request failed: " + textStatus );
		}); 
}

function copertinaRilevata(id)
{
	//faccio fare ancora un po' di movimento alla barra
	//$("#barraScansione").attr("src","assets/barra_verde.png");
	//devo far concludere il giro, o a destra o a sinistra
	stopBarAnim=true;
	idCopertinaRilevata=id;
}

function copertinaScelta(id)
{
	document.location = 'architectsdk://copertina'+id;
}

function creaImmagineCopertina(id)
{
	//vedo prima se ci sta la connessione, altrimenti è inutile caricare la thumb della copertina
	oggettoAttesaConnessione = new Object();
	oggettoAttesaConnessione.id = id;

	requestConnectionStatus();
}

function checkConnessione(status)
{
	console.log("tornato da check connessione, con status: " + status);
	var id =  oggettoAttesaConnessione.id;

	if (status==true) //connessione presente
	{

		var url=contenutiUrl+"/immaginiProgetti/thumb_"+id+".jpg";
		console.log(url);
		$("#barraScansione").hide();
		$("#messaggioIniziale").hide();
		$("#users_container").hide();

		$("#loader").show();

		//rimetto di base il contenitore al valore iniziale
		$("#imageContainer").css("width","90%").css("left","5%");
		// e anche l'immagine interna
		$("#imageContainerImg").css("width","100%");
		$("#imageContainerImg").css("height","initial");

		var larghezza=$( window )[0].innerWidth;
		var altezza=$( window )[0].innerHeight;

		var altezzaImmagineMax=altezza*0.5; //altezza massima immagine
		console.log("altezza massima: " + altezzaImmagineMax);
		//quanto lo devo fare grande l'immagine?
		var larghezzaDiv=$("#imageContainer").width();
		var htmlToAdd="";

		htmlToAdd += "<image id='imageContainerImg' onclick='copertinaScelta("+id+");' src='' style='width:100%;border: 0px;background: none;margin: 0px;box-shadow: none;margin-top: 0%;margin-bottom: 0%;' />";
		
		$("#imageContainer").html(htmlToAdd);
		console.log("Image container inner: " + htmlToAdd);

		var image = document.getElementById('imageContainerImg');
		var downloadingImage = new Image();

		downloadingImage.onload = function(){
			console.log("Ho caricato l'immagine");
			$("#imageContainer").css("border","1px solid "+ oggetto.coloreGenerale);
			$("#imageContainer").show();
			$("#loader").hide();
		    image.src = this.src;
		    //l'immagine riesce ad entrare nell'altezza massima?
			var altezzaImg=$("#imageContainerImg")[0].height;
			console.log("altezza effettiva immagine: " + altezzaImg);
			var posizione;
			if (altezzaImg<altezzaImmagineMax)
			{
				//tutto ok, la devo solo centrare
				posizione=(altezza-altezzaImg)/2;
			}
			else
			{
				//deve comandare l'altezza massima
				$("#imageContainerImg").css("height",altezzaImmagineMax+"px").css("width","initial");
				//vedo la larghezza effettiva
				var larghImg=$("#imageContainerImg")[0].width;
				// e setto il contenitore
				$("#imageContainer").css("width",larghImg+"px").css("left",((larghezza-larghImg)/2)+"px");
				posizione=(altezza-altezzaImmagineMax)/2;
				altezzaImg=$("#imageContainerImg")[0].height;
			}
			$("#imageContainer").css("top",posizione+"px");

			resizeBasedOnResolution(RA_COPERTINA)// per sicurezza

			setTimeout(function() {
				resizeBasedOnResolution(RA_COPERTINA)// per ulteriore sicurezza
			}, 1000);

			setTimeout(function() {
				resizeBasedOnResolution(RA_COPERTINA)// per ulteriore sicurezza
			}, 2000);

			//e fra 3 secondi la faccio andare a qul numero, se non ha premuto l'immagine
			setTimeout(function() {
				copertinaScelta(id)
			}, 3000);		
		};

		downloadingImage.onerror = function(){
			console.log("Errore nel caricamento dell'immagine");
			$("#loader").hide();
			copertinaScelta(id);
		};

		downloadingImage.src = url;

	}
	else //nessuna connessione, vado direttamente al numero senza mostrare la thumb
	{
		$("#loader").hide();
		copertinaScelta(id);
	}

}





$(document).ready(function()
{
//$("#loader").show();

	setTimeout(function() {
		resizeBasedOnResolution(RA_COPERTINA);
	}, 100);	

	setTimeout(function() {
		resizeBasedOnResolution(RA_COPERTINA);
	}, 1000);

return;
	var col1=convertHex("#00ff00",60);
	var col2=convertHex("#00ff00",30);
	var col3=convertHex("#00ff00",0);

	$( "#header" ).css("background","linear-gradient(to bottom, "+col1+", "+col2+"");
	$( "#subHeader" ).css("background","linear-gradient(to bottom, "+col2+", "+col3+"");
	$( "#footer" ).css("background","linear-gradient(to top, "+col1+", "+col3+"");
	

	resizeBasedOnResolution(RA_COPERTINA);

return;
	//testVideo();
	//testTesto();
	//scompareRettangolo();
	//muoviLineaDx();
	
	//testSlideshow();
	//$( "#rettangolo" ).hide();

/*
			$("#myPano").pano({
				img: "./panorama/sphere.jpg"
			});
*/
$("#messaggioIniziale").show();
$("#users_container").show();
$("#numeroUtenti").html(5);
//$("#bottoneNonRiesco").show();
resizeBasedOnResolution(RA_COPERTINA);
startAnimationBar();

});
	
