function Hotspot(oggetto,wiki_ar,parent){

   this.definizione = oggetto;
   this.arrayOverlay = [];
   this.arrayImages2Load = [];
   this.AR=wiki_ar;
   this.world=parent;
}

Hotspot.prototype.create = function (param)
{
	var dimensioneIndicatoreStandard=0.03;
	var dimensioneLabelStandard=0.05;

	//ciclo sull'array dei target e metto i punti
	for (i=0;i<this.definizione.target.length;i++)
	{

		var tipoIndicatore=this.definizione.target[i].tipoIndicatore;
		var coloreIndicatore=this.definizione.target[i].coloreIndicatore;

		if ((coloreIndicatore!="rosso") && (coloreIndicatore!="bianco") && (coloreIndicatore!="nero") && (coloreIndicatore!="giallo") && (coloreIndicatore!="blu") && (coloreIndicatore!="verde")) coloreIndicatore="rosso";
		if ((tipoIndicatore!="0") && (tipoIndicatore!="1") && (tipoIndicatore!="2") && (tipoIndicatore!="3") && (tipoIndicatore!="4") && (tipoIndicatore!="5") && (tipoIndicatore!="6") && (tipoIndicatore!="7")) tipoIndicatore="0";

		var immagineIndicatoreStandard="assets/"+"indicatore_"+tipoIndicatore+"_"+coloreIndicatore+".png";
		//console.log("Indicatore standard per questo target: " + immagineIndicatoreStandard);

		for (t=0;t<this.definizione.target[i].punti.length;t++)
		{
			console.log("considero il punto con label: " + this.definizione.target[i].punti[t].label);
			var dimensioneIndicatore=dimensioneIndicatoreStandard;
			var dimTemp = parseInt(this.definizione.target[i].punti[t].dimensione) || 0;
			if (dimTemp != 0) dimensioneIndicatore = dimTemp/100;

			var offsetGeneraleInizialeX=(Number(this.definizione.target[i].punti[t].x)/100)-0.5;
			var offsetGeneraleInizialeY=((Number(this.definizione.target[i].punti[t].y)/100)-0.5)*(-1);
			var aspectRatio = Number(this.definizione.target[i].aspectRatio); //visto che gli sostamenti sono in base all'altezza dell'immagine di riferimento, devo usare l'aspectRatio per correggere lo spostamento sulle x

			offsetGeneraleInizialeX=offsetGeneraleInizialeX*aspectRatio;

			var immagineIndicatore=immagineIndicatoreStandard;

			//questo HS ha un indicatore standard LEGATO al punto e non all'immagine? 
			if (this.definizione.target[i].punti[t].tipoIndicatore!="-1")
			{
				var tipoIndicatorePunto=this.definizione.target[i].punti[t].tipoIndicatore;
				var coloreIndicatorePunto=this.definizione.target[i].punti[t].coloreIndicatore;

				if ((coloreIndicatorePunto!="rosso") && (coloreIndicatorePunto!="bianco") && (coloreIndicatorePunto!="nero") && (coloreIndicatorePunto!="giallo") && (coloreIndicatorePunto!="blu") && (coloreIndicatorePunto!="verde")) coloreIndicatorePunto="rosso";
				if ((tipoIndicatorePunto!="0") && (tipoIndicatorePunto!="1") && (tipoIndicatorePunto!="2") && (tipoIndicatorePunto!="3") && (tipoIndicatorePunto!="4") && (tipoIndicatorePunto!="5") && (tipoIndicatorePunto!="6") && (tipoIndicatorePunto!="7")) tipoIndicatorePunto="0";

				immagineIndicatore="assets/"+"indicatore_"+tipoIndicatorePunto+"_"+coloreIndicatorePunto+".png";
				//console.log("Indicatore standard (legato al punto) per questo target: " + immagineIndicatore);
			}

			//è un indicatore custom o standard (vecchio stile)?
			if (this.definizione.target[i].punti[t].pathIndicatore)
			{
				//console.log("il punto "+t+" ha un indicatore custom");
				immagineIndicatore = contenutiPath + this.definizione.target[i].punti[t].pathIndicatore;
			}

			//è un indicatore custom nuovo stile?
			var overlay;
			if (this.definizione.target[i].punti[t].hsc_tipo)
			{
				//console.log("E' un hotspot custom nuovo stile, di tipo: " + this.definizione.target[i].punti[t].hsc_tipo);

				if (this.definizione.target[i].punti[t].hsc_tipo==1) //modello 3d
				{
					var pathModello = contenutiPath + this.definizione.target[i].punti[t].hsc_path;
					//console.log("path modello: " + pathModello);
					//var dimIniziale=parseFloat(1/this.definizione.target[i].punti[t].hsc_sdu);
					var dimIniziale=(Number(1/this.definizione.target[i].punti[t].hsc_sdu))*dimensioneIndicatore;
					var rotX=Number(this.definizione.target[i].punti[t].hsc_rotx);
					var rotY=Number(this.definizione.target[i].punti[t].hsc_roty);
					var rotZ=Number(this.definizione.target[i].punti[t].hsc_rotz);
					var toRotate=false;
					var rotazionePeriodo=10;
					if (this.definizione.target[i].punti[t].rotazione=="1")
					{
						toRotate=true;
						rotazionePeriodo=parseInt(this.definizione.target[i].punti[t].periodo_rotazione);
					}

					//console.log("dimensione iniziale: " + dimIniziale);
					//console.log("rotation x: " + rotX);
					//console.log("rotation y: " + rotY);
					//console.log("rotation z: " + rotZ);
					//if (toRotate) console.log("periodo rotazione: " + rotazionePeriodo);

					overlay = new this.AR.Model(pathModello, {
						scale: {
							x: dimIniziale,
							y: dimIniziale,
							z: dimIniziale
						},
						rotate: {
							x: rotX,
							y: rotY,
							z: rotZ
						},
						translate: {
							x: offsetGeneraleInizialeX,
							y: offsetGeneraleInizialeY,
							z: 0
						},
						enabled: false
					});
					overlay.onClick = this.world.mostraContenuti(i,t);
					overlay.customType=this.definizione.target[i].punti[t].hsc_tipo;
					overlay.targetRef=this.definizione.target[i].targetName;
					if (toRotate)
					{
						overlay.rotationAnimation = new this.AR.PropertyAnimation(overlay, "rotate.z", 0, 360, rotazionePeriodo*1000);
					}
					this.arrayOverlay.push(overlay);
				}
				else if (this.definizione.target[i].punti[t].hsc_tipo==0) //immagine
				{
					immagineIndicatore = contenutiPath + this.definizione.target[i].punti[t].hsc_path;
					var imgTemp = new this.AR.ImageResource(immagineIndicatore);
					overlay = new this.AR.ImageDrawable(imgTemp, dimensioneIndicatore,
					{
						offsetX: offsetGeneraleInizialeX,
						offsetY: offsetGeneraleInizialeY,
						scale: 1,
						enabled: false,
						opacity : 1.0,
						zOrder: 10
					});
					overlay.onClick = this.world.mostraContenuti(i,t);
					overlay.customType=this.definizione.target[i].punti[t].hsc_tipo;
					overlay.targetRef=this.definizione.target[i].targetName;
					this.arrayOverlay.push(overlay);						
				}
			}				
			else
			{	
				var imgTemp = new this.AR.ImageResource(immagineIndicatore);
				overlay = new this.AR.ImageDrawable(imgTemp, dimensioneIndicatore,
				{
					offsetX: offsetGeneraleInizialeX,
					offsetY: offsetGeneraleInizialeY,
					scale: 1,
					enabled: false,
					opacity : 1.0,
					zOrder: 10
				});
				overlay.onClick = this.world.mostraContenuti(i,t);
				overlay.customType=-1;
				overlay.targetRef=this.definizione.target[i].targetName;
				this.arrayOverlay.push(overlay);
			}

			var dimensioneLabel=dimensioneLabelStandard;
			var dimTemp = parseInt(this.definizione.target[i].punti[t].labelDimension) || 0;
			if (dimTemp != 0) dimensioneLabel = dimTemp/100;
			var labelColor=this.definizione.target[i].punti[t].labelColor;
			var labelSfondoColor=this.definizione.target[i].punti[t].labelSfondoCol;
			var labelSfondoTrasp=parseInt(this.definizione.target[i].punti[t].labelSfondoTrasp);
			var labelStyle=parseInt(this.definizione.target[i].punti[t].labelStile);
			//creo il colore con l'opacità per lo sfondo della label
			//trasformo il valore 0-100 in 0-255
			var trasp0255=parseInt(255*labelSfondoTrasp/100);
			var hexString = trasp0255.toString(16);
			var labelSfondoColor=labelSfondoColor+hexString;
			//alert(labelSfondoColor);
			//stile
			var myFontstyle=AR.CONST.FONT_STYLE.NORMAL;
			if (labelStyle==1) myFontstyle=AR.CONST.FONT_STYLE.BOLD;
			else if (labelStyle==2) myFontstyle=AR.CONST.FONT_STYLE.ITALIC;

			var label = new this.AR.Label(this.definizione.target[i].punti[t].label, dimensioneLabel, {
				//offsetX: this.definizione.immagini[i].elementi[t].placeholderOffsetX+0.10,
				//offsetX: offsetGeneraleInizialeX,
				//offsetY: offsetGeneraleInizialeY-(0.06*1),
				scale: 1,
				enabled: false,
				opacity : 1.0,
				zOrder: 15,
				//horizontalAnchor : this.AR.CONST.HORIZONTAL_ANCHOR.LEFT,
				style : {
					textColor : labelColor,
					backgroundColor : labelSfondoColor,
					fontStyle:myFontstyle
				}

			});
			label.onClick = this.world.mostraContenuti(i,t);
			label.customType=-1;
			label.immagineRef=i;
			label.puntoRef=t;
			label.targetRef=this.definizione.target[i].targetName;

			var labelPosition=this.definizione.target[i].punti[t].labelPosition;
			//console.log("Label: " + this.definizione.target[i].punti[t].label + " - posizione: " + labelPosition);
			if (labelPosition=="up")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.CENTER;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.BOTTOM;
				label.offsetX=offsetGeneraleInizialeX;
				label.offsetY=offsetGeneraleInizialeY+(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="down")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.CENTER;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.TOP;
				label.offsetX=offsetGeneraleInizialeX;
				label.offsetY=offsetGeneraleInizialeY-(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="sx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.MIDDLE;
				label.offsetX=offsetGeneraleInizialeX-(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY;
			}
			else if (labelPosition=="dx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.MIDDLE;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY;
			}
			else if (labelPosition=="upDx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.BOTTOM;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY+(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="downDx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.TOP;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY-(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="downSx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.TOP;
				label.offsetX=offsetGeneraleInizialeX-(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY-(dimensioneIndicatore*0.6);
			}
			else if (labelPosition=="upSx")
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.RIGHT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.BOTTOM;
				label.offsetX=offsetGeneraleInizialeX-(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY+(dimensioneIndicatore*0.6);
			}
			else //di default a destra
			{
				label.horizontalAnchor=this.AR.CONST.HORIZONTAL_ANCHOR.LEFT;
				label.verticalAnchor=this.AR.CONST.VERTICAL_ANCHOR.MIDDLE;
				label.offsetX=offsetGeneraleInizialeX+(dimensioneIndicatore*0.6);
				label.offsetY=offsetGeneraleInizialeY;
			}


			this.arrayOverlay.push(label);
			
		}
	}
  
}


Hotspot.prototype.getArrayOverlay = function ()
{
	return this.arrayOverlay;
}

Hotspot.prototype.getArrayImages2Load = function ()
{
	return this.arrayImages2Load;
}


