var queryStringArray= new Array();

function doRequire(array,indice)
{
  var className=array[indice]+"_"+oggetto.testataSocket;
  var localFileUrl=dynClassesPath+className+".js";
  //console.log("Sono nel doRequire, per la classe: " + className + " - " + localFileUrl);
  try {
  requirejs(
      [ localFileUrl ],
      function() {
          //console.log("Tutto ok");
          if (className.indexOf("DynamicPos") !== -1)
          {
            dynamicPosClass = new window[className]();
            console.log("Instanziata la classe dinamica del posizionamento");
          }
          else
          {
            var dataObj = new Object();
            dataObj.contenutiUrl=contenutiUrl;
            dataObj.oggetto=oggetto.target;
            dataObj.showCommentsDisclaimer=showCommentsDisclaimer;
            dataObj.commentsNickname=commentsNickname;

            console.log(array[indice]);
            if (array[indice]=="Calcolomatriciale") arrayDynamicClasses.push(new window[className]($("#dynamicClassContainer"),dataObj,closeContenutoDynExt));
            else arrayDynamicClasses.push(new window[className]($("#dynamicClassContainer"),dataObj,closeContenutoDyn));
            console.log("Instanziata la classe dinamica : " + className);
          }
          //continuo?
          if (indice==(array.length)-1)
          {
            console.log("faccio partire Wiki....");
        setTimeout(function() {
          World.init();
        }, 500);
          }
          else
          {
            //console.log("continuo a inizializzare le classi dinamiche....");
        doRequire(array,indice+1);
          }
      },
      function(err) {
          console.log("errore durante il require: " + err);
          //alert("errore durante il require: " + err + " - indice: " + indice + " - lunghezza: " + array.length);
          //errore nel require di quella classe
          //continuo?
          if (indice==(array.length)-1)
          {
            console.log("faccio partire Wiki....");
        setTimeout(function() {
          World.init();
        }, 500);
          }
          else
          {
            //console.log("continuo a inizializzare le classi dinamiche....");
        doRequire(array,indice+1);
          }
      }
  );

  } catch(err) {
        console.log("catch durante il require: " + err);
    //errore nel require di quella classe
        //continuo?
        if (indice==(array.length)-1)
        {
          //console.log("faccio partire Wiki....");
      setTimeout(function() {
        //alert("init4");
        World.init();
      }, 500);
        }
        else
        {
          console.log("continuo a inizializzare le classi dinamiche....");
      doRequire(array,indice+1);
        }
  }
}

function closeContenutoDyn()
{
  //alert("sono qui");
  setTimeout(function() {
    AR.hardware.camera.enabled = true;
    back();
  }, 500);
}

function closeContenutoDynExt(param)
{
  console.log("sono in closeContenutoDynExt");
  console.log(JSON.stringify(param));
  if (param.code=="Calcolomatriciale")
  {
    //i parametri sono validi?
    if ((param.oggetto.colonna!=-1) && (param.oggetto.riga!=-1) && (param.oggetto.parametro!=-1))
    {
      var queryString="c_"+param.oggetto.idHS+"="+param.oggetto.colonna+"&r_"+param.oggetto.idHS+"="+param.oggetto.riga+"&p_"+param.oggetto.idHS+"="+param.oggetto.parametro;
      console.log("querystring: " + queryString);
      // ci sta un altro item nella query string array che è relativo allo stesso punto (nuovo calcolo sullo stesso punto...)
      var trovato=false;
      for (var i=0;i<queryStringArray.length;i++)
      {
        if (queryStringArray[i].startsWith("c_"+param.oggetto.idHS+"="))
        {
          //trovato già un item
          queryStringArray[i] = queryString;
          trovato=true;
        }
      }
      if (!trovato) queryStringArray.push(queryString);
      console.log("queryStringArray: " + JSON.stringify(queryStringArray));
      // e modifico la label
      modificaLabel(param.oggetto.idImmagine, param.oggetto.idPunto, param.oggetto.result);

    }


  }



  //alert("sono qui");
  setTimeout(function() {
    AR.hardware.camera.enabled = true;
    back();
  }, 500);
}

function commentReturn(oggetto)
{
  var decodedJson = decodeURIComponent(oggetto);
    decodedJson = decodedJson.replace(/\|/g, "'"); //per rimettere eventuali apici

  var obj= JSON.parse(decodedJson);

  if (obj.type=="SEND") actualClass.inviaMessaggioControllato(obj);
  else actualClass.mostraMessaggi();
}